# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-29 16:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bar', '0002_auto_20170529_1632'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='stock',
            new_name='master',
        ),
    ]
