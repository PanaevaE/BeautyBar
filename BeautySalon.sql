-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2017 at 07:57 AM
-- Server version: 5.7.16-0ubuntu0.16.10.1
-- PHP Version: 7.0.8-3ubuntu3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BeautySalon`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can use Structure mode', 1, 'use_structure'),
(2, 'Can change page', 2, 'change_page'),
(3, 'Can add user setting', 3, 'add_usersettings'),
(4, 'Can change user setting', 3, 'change_usersettings'),
(5, 'Can delete user setting', 3, 'delete_usersettings'),
(6, 'Can add page', 2, 'add_page'),
(7, 'Can delete page', 2, 'delete_page'),
(8, 'Can view page', 2, 'view_page'),
(9, 'Can publish page', 2, 'publish_page'),
(10, 'Can edit static placeholders', 2, 'edit_static_placeholder'),
(11, 'Can add Page global permission', 4, 'add_globalpagepermission'),
(12, 'Can change Page global permission', 4, 'change_globalpagepermission'),
(13, 'Can delete Page global permission', 4, 'delete_globalpagepermission'),
(14, 'Can add Page permission', 5, 'add_pagepermission'),
(15, 'Can change Page permission', 5, 'change_pagepermission'),
(16, 'Can delete Page permission', 5, 'delete_pagepermission'),
(17, 'Can add User (page)', 6, 'add_pageuser'),
(18, 'Can change User (page)', 6, 'change_pageuser'),
(19, 'Can delete User (page)', 6, 'delete_pageuser'),
(20, 'Can add User group (page)', 7, 'add_pageusergroup'),
(21, 'Can change User group (page)', 7, 'change_pageusergroup'),
(22, 'Can delete User group (page)', 7, 'delete_pageusergroup'),
(23, 'Can add placeholder', 1, 'add_placeholder'),
(24, 'Can change placeholder', 1, 'change_placeholder'),
(25, 'Can delete placeholder', 1, 'delete_placeholder'),
(26, 'Can add cms plugin', 8, 'add_cmsplugin'),
(27, 'Can change cms plugin', 8, 'change_cmsplugin'),
(28, 'Can delete cms plugin', 8, 'delete_cmsplugin'),
(29, 'Can add title', 9, 'add_title'),
(30, 'Can change title', 9, 'change_title'),
(31, 'Can delete title', 9, 'delete_title'),
(32, 'Can add placeholder reference', 10, 'add_placeholderreference'),
(33, 'Can change placeholder reference', 10, 'change_placeholderreference'),
(34, 'Can delete placeholder reference', 10, 'delete_placeholderreference'),
(35, 'Can add static placeholder', 11, 'add_staticplaceholder'),
(36, 'Can change static placeholder', 11, 'change_staticplaceholder'),
(37, 'Can delete static placeholder', 11, 'delete_staticplaceholder'),
(38, 'Can add alias plugin model', 12, 'add_aliaspluginmodel'),
(39, 'Can change alias plugin model', 12, 'change_aliaspluginmodel'),
(40, 'Can delete alias plugin model', 12, 'delete_aliaspluginmodel'),
(41, 'Can add urlconf revision', 13, 'add_urlconfrevision'),
(42, 'Can change urlconf revision', 13, 'change_urlconfrevision'),
(43, 'Can delete urlconf revision', 13, 'delete_urlconfrevision'),
(44, 'Can add cache key', 14, 'add_cachekey'),
(45, 'Can change cache key', 14, 'change_cachekey'),
(46, 'Can delete cache key', 14, 'delete_cachekey'),
(47, 'Can add permission', 15, 'add_permission'),
(48, 'Can change permission', 15, 'change_permission'),
(49, 'Can delete permission', 15, 'delete_permission'),
(50, 'Can add group', 16, 'add_group'),
(51, 'Can change group', 16, 'change_group'),
(52, 'Can delete group', 16, 'delete_group'),
(53, 'Can add user', 17, 'add_user'),
(54, 'Can change user', 17, 'change_user'),
(55, 'Can delete user', 17, 'delete_user'),
(56, 'Can add content type', 18, 'add_contenttype'),
(57, 'Can change content type', 18, 'change_contenttype'),
(58, 'Can delete content type', 18, 'delete_contenttype'),
(59, 'Can add session', 19, 'add_session'),
(60, 'Can change session', 19, 'change_session'),
(61, 'Can delete session', 19, 'delete_session'),
(62, 'Can add log entry', 20, 'add_logentry'),
(63, 'Can change log entry', 20, 'change_logentry'),
(64, 'Can delete log entry', 20, 'delete_logentry'),
(65, 'Can add site', 21, 'add_site'),
(66, 'Can change site', 21, 'change_site'),
(67, 'Can delete site', 21, 'delete_site'),
(68, 'Can add flash', 22, 'add_flash'),
(69, 'Can change flash', 22, 'change_flash'),
(70, 'Can delete flash', 22, 'delete_flash'),
(71, 'Can add Folder', 23, 'add_folder'),
(72, 'Can change Folder', 23, 'change_folder'),
(73, 'Can delete Folder', 23, 'delete_folder'),
(74, 'Can use directory listing', 23, 'can_use_directory_listing'),
(75, 'Can add folder permission', 24, 'add_folderpermission'),
(76, 'Can change folder permission', 24, 'change_folderpermission'),
(77, 'Can delete folder permission', 24, 'delete_folderpermission'),
(78, 'Can add file', 25, 'add_file'),
(79, 'Can change file', 25, 'change_file'),
(80, 'Can delete file', 25, 'delete_file'),
(81, 'Can add clipboard', 26, 'add_clipboard'),
(82, 'Can change clipboard', 26, 'change_clipboard'),
(83, 'Can delete clipboard', 26, 'delete_clipboard'),
(84, 'Can add clipboard item', 27, 'add_clipboarditem'),
(85, 'Can change clipboard item', 27, 'change_clipboarditem'),
(86, 'Can delete clipboard item', 27, 'delete_clipboarditem'),
(87, 'Can add image', 28, 'add_image'),
(88, 'Can change image', 28, 'change_image'),
(89, 'Can delete image', 28, 'delete_image'),
(90, 'Can add thumbnail option', 29, 'add_thumbnailoption'),
(91, 'Can change thumbnail option', 29, 'change_thumbnailoption'),
(92, 'Can delete thumbnail option', 29, 'delete_thumbnailoption'),
(93, 'Can add file', 30, 'add_file'),
(94, 'Can change file', 30, 'change_file'),
(95, 'Can delete file', 30, 'delete_file'),
(96, 'Can add folder', 31, 'add_folder'),
(97, 'Can change folder', 31, 'change_folder'),
(98, 'Can delete folder', 31, 'delete_folder'),
(99, 'Can add source', 32, 'add_source'),
(100, 'Can change source', 32, 'change_source'),
(101, 'Can delete source', 32, 'delete_source'),
(102, 'Can add thumbnail', 33, 'add_thumbnail'),
(103, 'Can change thumbnail', 33, 'change_thumbnail'),
(104, 'Can delete thumbnail', 33, 'delete_thumbnail'),
(105, 'Can add thumbnail dimensions', 34, 'add_thumbnaildimensions'),
(106, 'Can change thumbnail dimensions', 34, 'change_thumbnaildimensions'),
(107, 'Can delete thumbnail dimensions', 34, 'delete_thumbnaildimensions'),
(108, 'Can add google map', 35, 'add_googlemap'),
(109, 'Can change google map', 35, 'change_googlemap'),
(110, 'Can delete google map', 35, 'delete_googlemap'),
(111, 'Can add google map marker', 36, 'add_googlemapmarker'),
(112, 'Can change google map marker', 36, 'change_googlemapmarker'),
(113, 'Can delete google map marker', 36, 'delete_googlemapmarker'),
(114, 'Can add google map route', 37, 'add_googlemaproute'),
(115, 'Can change google map route', 37, 'change_googlemaproute'),
(116, 'Can delete google map route', 37, 'delete_googlemaproute'),
(117, 'Can add link', 38, 'add_link'),
(118, 'Can change link', 38, 'change_link'),
(119, 'Can delete link', 38, 'delete_link'),
(120, 'Can add picture', 39, 'add_picture'),
(121, 'Can change picture', 39, 'change_picture'),
(122, 'Can delete picture', 39, 'delete_picture'),
(123, 'Can add Snippet', 40, 'add_snippet'),
(124, 'Can change Snippet', 40, 'change_snippet'),
(125, 'Can delete Snippet', 40, 'delete_snippet'),
(126, 'Can add Snippet', 41, 'add_snippetptr'),
(127, 'Can change Snippet', 41, 'change_snippetptr'),
(128, 'Can delete Snippet', 41, 'delete_snippetptr'),
(129, 'Can add teaser', 42, 'add_teaser'),
(130, 'Can change teaser', 42, 'change_teaser'),
(131, 'Can delete teaser', 42, 'delete_teaser'),
(132, 'Can add text', 43, 'add_text'),
(133, 'Can change text', 43, 'change_text'),
(134, 'Can delete text', 43, 'delete_text'),
(135, 'Can add video player', 44, 'add_videoplayer'),
(136, 'Can change video player', 44, 'change_videoplayer'),
(137, 'Can delete video player', 44, 'delete_videoplayer'),
(138, 'Can add video source', 45, 'add_videosource'),
(139, 'Can change video source', 45, 'change_videosource'),
(140, 'Can delete video source', 45, 'delete_videosource'),
(141, 'Can add video track', 46, 'add_videotrack'),
(142, 'Can change video track', 46, 'change_videotrack'),
(143, 'Can delete video track', 46, 'delete_videotrack'),
(144, 'Can add twitter recent entries', 47, 'add_twitterrecententries'),
(145, 'Can change twitter recent entries', 47, 'change_twitterrecententries'),
(146, 'Can delete twitter recent entries', 47, 'delete_twitterrecententries'),
(147, 'Can add twitter search', 48, 'add_twittersearch'),
(148, 'Can change twitter search', 48, 'change_twittersearch'),
(149, 'Can delete twitter search', 48, 'delete_twittersearch'),
(150, 'Can add css background', 49, 'add_cssbackground'),
(151, 'Can change css background', 49, 'change_cssbackground'),
(152, 'Can delete css background', 49, 'delete_cssbackground'),
(153, 'Can add filer css background', 50, 'add_filercssbackground'),
(154, 'Can change filer css background', 50, 'change_filercssbackground'),
(155, 'Can delete filer css background', 50, 'delete_filercssbackground');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$20000$stkh1NTuG23m$RGuAc7QJZ8D4dnBRcWdpNurHuEY5hDz7QBg1m+dBXOI=', '2017-01-19 02:50:51.757081', 1, 'root', '', '', 'patya_katya@mail.ru', 1, 1, '2017-01-14 17:16:14.965392'),
(2, 'pbkdf2_sha256$24000$LUgSOPIPoDr7$v8S+j6bwIEqOUYrJIvLcGSoPNhkQ9N7oYXKqbFuk0Zs=', NULL, 1, 'kate', '', '', 'kate@admin.ru', 1, 1, '2017-01-15 16:52:48.693751');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmsplugin_css_background_cssbackground`
--

CREATE TABLE `cmsplugin_css_background_cssbackground` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `color` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `repeat` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `bg_position` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `forced` tinyint(1) NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmsplugin_css_background_filercssbackground`
--

CREATE TABLE `cmsplugin_css_background_filercssbackground` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `color` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `repeat` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `bg_position` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `forced` tinyint(1) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_aliaspluginmodel`
--

CREATE TABLE `cms_aliaspluginmodel` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `plugin_id` int(11) DEFAULT NULL,
  `alias_placeholder_id` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_cmsplugin`
--

CREATE TABLE `cms_cmsplugin` (
  `id` int(11) NOT NULL,
  `position` smallint(5) UNSIGNED NOT NULL,
  `language` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `plugin_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `changed_date` datetime(6) NOT NULL,
  `parent_id` int(11),
  `placeholder_id` int(11),
  `depth` int(10) UNSIGNED NOT NULL,
  `numchild` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_cmsplugin`
--

INSERT INTO `cms_cmsplugin` (`id`, `position`, `language`, `plugin_type`, `creation_date`, `changed_date`, `parent_id`, `placeholder_id`, `depth`, `numchild`, `path`) VALUES
(2, 0, 'ru', 'TeaserPlugin', '2017-01-15 18:00:31.432112', '2017-01-15 18:00:31.444143', NULL, 2, 1, 0, '0002'),
(5, 0, 'ru', 'TextPlugin', '2017-01-15 18:16:49.994633', '2017-01-15 18:18:36.527816', NULL, 3, 1, 0, '0005'),
(6, 0, 'ru', 'TeaserPlugin', '2017-01-15 18:00:31.432112', '2017-01-15 18:18:45.503109', NULL, 4, 1, 0, '0006'),
(7, 0, 'ru', 'TextPlugin', '2017-01-15 18:16:49.994633', '2017-01-15 18:18:46.058596', NULL, 5, 1, 0, '0007'),
(10, 0, 'ru', 'PicturePlugin', '2017-01-16 17:15:05.796979', '2017-01-16 17:15:05.839683', NULL, 18, 1, 0, '0008'),
(11, 0, 'ru', 'PicturePlugin', '2017-01-16 17:22:09.250861', '2017-01-16 17:22:09.275442', NULL, 20, 1, 0, '0009'),
(12, 0, 'ru', 'TextPlugin', '2017-01-16 17:31:08.807530', '2017-01-16 17:31:08.846514', NULL, 21, 1, 0, '000A'),
(13, 0, 'ru', 'PicturePlugin', '2017-01-16 17:34:58.681708', '2017-01-16 17:34:58.694698', NULL, 21, 1, 0, '000B'),
(14, 0, 'ru', 'PicturePlugin', '2017-01-16 17:37:51.007816', '2017-01-16 17:48:45.812906', NULL, 22, 1, 0, '000C'),
(22, 0, 'ru', 'LinkPlugin', '2017-01-17 18:16:24.482539', '2017-01-17 18:16:24.497778', NULL, 29, 1, 0, '000K'),
(23, 1, 'ru', 'PicturePlugin', '2017-01-17 18:36:57.471462', '2017-01-17 18:36:57.486355', NULL, 30, 1, 0, '000L'),
(24, 0, 'ru', 'PicturePlugin', '2017-01-17 18:42:50.204312', '2017-01-17 18:42:50.222960', NULL, 31, 1, 0, '000M'),
(26, 0, 'ru', 'PicturePlugin', '2017-01-18 18:56:05.381677', '2017-01-18 18:56:05.403319', NULL, 49, 1, 0, '000O'),
(27, 0, 'ru', 'PicturePlugin', '2017-01-18 18:58:42.622537', '2017-01-18 18:58:42.646372', NULL, 50, 1, 0, '000P'),
(28, 0, 'ru', 'PicturePlugin', '2017-01-18 19:05:59.373186', '2017-01-18 19:05:59.396848', NULL, 51, 1, 0, '000R'),
(40, 0, 'ru', 'PicturePlugin', '2017-01-18 19:25:43.809500', '2017-01-18 19:25:43.825673', NULL, 52, 1, 0, '0013'),
(41, 0, 'ru', 'PicturePlugin', '2017-01-18 19:26:18.660289', '2017-01-18 19:26:18.677510', NULL, 53, 1, 0, '0014'),
(42, 0, 'ru', 'PicturePlugin', '2017-01-18 19:27:35.712107', '2017-01-18 19:27:35.741068', NULL, 54, 1, 0, '0015'),
(43, 0, 'ru', 'PicturePlugin', '2017-01-18 19:28:25.072001', '2017-01-18 19:28:25.088601', NULL, 55, 1, 0, '0016'),
(44, 0, 'ru', 'PicturePlugin', '2017-01-18 19:30:16.651224', '2017-01-18 19:30:16.690385', NULL, 56, 1, 0, '0017'),
(45, 0, 'ru', 'PicturePlugin', '2017-01-18 19:31:32.128741', '2017-01-18 19:31:32.145562', NULL, 1, 1, 0, '0018'),
(46, 0, 'ru', 'PicturePlugin', '2017-01-18 19:32:33.679744', '2017-01-18 19:32:33.692808', NULL, 57, 1, 0, '0019'),
(47, 0, 'ru', 'PicturePlugin', '2017-01-18 19:33:12.679116', '2017-01-18 19:33:12.694153', NULL, 58, 1, 0, '001A'),
(48, 0, 'ru', 'PicturePlugin', '2017-01-18 19:34:00.766909', '2017-01-18 19:34:00.789717', NULL, 59, 1, 0, '001B'),
(49, 0, 'ru', 'PicturePlugin', '2017-01-18 19:34:53.016403', '2017-01-18 19:34:53.041765', NULL, 60, 1, 0, '001C'),
(50, 0, 'ru', 'PicturePlugin', '2017-01-18 20:14:37.590965', '2017-01-18 20:14:37.617054', NULL, 61, 1, 0, '001D'),
(51, 0, 'ru', 'PicturePlugin', '2017-01-18 20:15:33.100215', '2017-01-18 20:15:33.114378', NULL, 62, 1, 0, '001E'),
(52, 0, 'ru', 'PicturePlugin', '2017-01-18 20:17:04.402492', '2017-01-18 20:17:04.418636', NULL, 63, 1, 0, '001F'),
(53, 0, 'ru', 'PicturePlugin', '2017-01-18 20:18:10.381645', '2017-01-18 20:18:10.398292', NULL, 97, 1, 0, '001G'),
(54, 0, 'ru', 'PicturePlugin', '2017-01-18 20:18:51.057516', '2017-01-18 20:18:51.071790', NULL, 98, 1, 0, '001H'),
(55, 0, 'ru', 'PicturePlugin', '2017-01-18 20:19:34.499545', '2017-01-18 20:19:34.513785', NULL, 64, 1, 0, '001I'),
(56, 0, 'ru', 'PicturePlugin', '2017-01-18 20:20:17.320756', '2017-01-18 20:20:17.333781', NULL, 71, 1, 0, '001J'),
(57, 0, 'ru', 'PicturePlugin', '2017-01-18 20:24:34.103657', '2017-01-18 20:24:34.139582', NULL, 65, 1, 0, '001K'),
(58, 0, 'ru', 'PicturePlugin', '2017-01-18 20:25:49.875253', '2017-01-18 20:25:49.893207', NULL, 66, 1, 0, '001L'),
(59, 0, 'ru', 'PicturePlugin', '2017-01-18 20:26:39.566826', '2017-01-18 20:26:39.585365', NULL, 67, 1, 0, '001M'),
(60, 0, 'ru', 'PicturePlugin', '2017-01-18 20:27:36.747764', '2017-01-18 20:27:36.765181', NULL, 68, 1, 0, '001N'),
(61, 0, 'ru', 'PicturePlugin', '2017-01-18 20:28:34.465511', '2017-01-18 20:28:34.481970', NULL, 69, 1, 0, '001O'),
(62, 0, 'ru', 'PicturePlugin', '2017-01-18 20:29:23.205548', '2017-01-18 20:29:23.219906', NULL, 70, 1, 0, '001P'),
(96, 0, 'ru', 'PicturePlugin', '2017-01-19 02:52:11.864784', '2017-01-19 02:52:11.888176', NULL, 101, 1, 0, '002N'),
(97, 0, 'ru', 'PicturePlugin', '2017-01-19 02:54:18.107027', '2017-01-19 02:54:18.146247', NULL, 102, 1, 0, '002O'),
(98, 0, 'ru', 'PicturePlugin', '2017-01-16 17:15:05.796979', '2017-01-19 02:54:56.195744', NULL, 19, 1, 0, '002P'),
(99, 0, 'ru', 'PicturePlugin', '2017-01-16 17:22:09.250861', '2017-01-19 02:54:56.271690', NULL, 28, 1, 0, '002Q'),
(100, 0, 'ru', 'TextPlugin', '2017-01-19 02:54:56.298928', '2017-01-19 02:54:56.308587', NULL, 23, 1, 0, '002R'),
(101, 0, 'ru', 'PicturePlugin', '2017-01-16 17:34:58.681708', '2017-01-19 02:54:56.325443', NULL, 23, 1, 0, '002S'),
(102, 0, 'ru', 'PicturePlugin', '2017-01-16 17:37:51.007816', '2017-01-19 02:54:56.374136', NULL, 24, 1, 0, '002T'),
(103, 0, 'ru', 'LinkPlugin', '2017-01-17 18:16:24.482539', '2017-01-19 02:54:56.416097', NULL, 44, 1, 0, '002U'),
(104, 1, 'ru', 'PicturePlugin', '2017-01-17 18:36:57.471462', '2017-01-19 02:54:56.450155', NULL, 45, 1, 0, '002V'),
(105, 0, 'ru', 'PicturePlugin', '2017-01-17 18:42:50.204312', '2017-01-19 02:54:56.490972', NULL, 46, 1, 0, '002W'),
(106, 0, 'ru', 'PicturePlugin', '2017-01-18 18:56:05.381677', '2017-01-19 02:54:56.537107', NULL, 72, 1, 0, '002X'),
(107, 0, 'ru', 'PicturePlugin', '2017-01-18 18:58:42.622537', '2017-01-19 02:54:56.578813', NULL, 73, 1, 0, '002Y'),
(108, 0, 'ru', 'PicturePlugin', '2017-01-18 19:05:59.373186', '2017-01-19 02:54:56.622464', NULL, 74, 1, 0, '002Z'),
(109, 0, 'ru', 'PicturePlugin', '2017-01-18 19:25:43.809500', '2017-01-19 02:54:56.664565', NULL, 75, 1, 0, '0030'),
(110, 0, 'ru', 'PicturePlugin', '2017-01-18 19:26:18.660289', '2017-01-19 02:54:56.703202', NULL, 76, 1, 0, '0031'),
(111, 0, 'ru', 'PicturePlugin', '2017-01-18 19:27:35.712107', '2017-01-19 02:54:56.742542', NULL, 77, 1, 0, '0032'),
(112, 0, 'ru', 'PicturePlugin', '2017-01-18 19:28:25.072001', '2017-01-19 02:54:56.782254', NULL, 78, 1, 0, '0033'),
(113, 0, 'ru', 'PicturePlugin', '2017-01-18 19:30:16.651224', '2017-01-19 02:54:56.840181', NULL, 79, 1, 0, '0034'),
(114, 0, 'ru', 'PicturePlugin', '2017-01-18 19:32:33.679744', '2017-01-19 02:54:56.885526', NULL, 80, 1, 0, '0035'),
(115, 0, 'ru', 'PicturePlugin', '2017-01-18 19:33:12.679116', '2017-01-19 02:54:56.930876', NULL, 81, 1, 0, '0036'),
(116, 0, 'ru', 'PicturePlugin', '2017-01-18 19:34:00.766909', '2017-01-19 02:54:56.983152', NULL, 82, 1, 0, '0037'),
(117, 0, 'ru', 'PicturePlugin', '2017-01-18 19:34:53.016403', '2017-01-19 02:54:57.032750', NULL, 83, 1, 0, '0038'),
(118, 0, 'ru', 'PicturePlugin', '2017-01-18 20:14:37.590965', '2017-01-19 02:54:57.072357', NULL, 84, 1, 0, '0039'),
(119, 0, 'ru', 'PicturePlugin', '2017-01-18 20:15:33.100215', '2017-01-19 02:54:57.140541', NULL, 85, 1, 0, '003A'),
(120, 0, 'ru', 'PicturePlugin', '2017-01-18 20:17:04.402492', '2017-01-19 02:54:57.195839', NULL, 86, 1, 0, '003B'),
(121, 0, 'ru', 'PicturePlugin', '2017-01-18 20:19:34.499545', '2017-01-19 02:54:57.273800', NULL, 87, 1, 0, '003C'),
(122, 0, 'ru', 'PicturePlugin', '2017-01-18 20:24:34.103657', '2017-01-19 02:54:57.354550', NULL, 88, 1, 0, '003D'),
(123, 0, 'ru', 'PicturePlugin', '2017-01-18 20:25:49.875253', '2017-01-19 02:54:57.400097', NULL, 89, 1, 0, '003E'),
(124, 0, 'ru', 'PicturePlugin', '2017-01-18 20:26:39.566826', '2017-01-19 02:54:57.469944', NULL, 90, 1, 0, '003F'),
(125, 0, 'ru', 'PicturePlugin', '2017-01-18 20:27:36.747764', '2017-01-19 02:54:57.541724', NULL, 91, 1, 0, '003G'),
(126, 0, 'ru', 'PicturePlugin', '2017-01-18 20:28:34.465511', '2017-01-19 02:54:57.577027', NULL, 92, 1, 0, '003H'),
(127, 0, 'ru', 'PicturePlugin', '2017-01-18 20:29:23.205548', '2017-01-19 02:54:57.614208', NULL, 93, 1, 0, '003I'),
(128, 0, 'ru', 'PicturePlugin', '2017-01-18 20:20:17.320756', '2017-01-19 02:54:57.647223', NULL, 94, 1, 0, '003J'),
(129, 0, 'ru', 'PicturePlugin', '2017-01-18 20:18:10.381645', '2017-01-19 02:54:57.681337', NULL, 99, 1, 0, '003K'),
(130, 0, 'ru', 'PicturePlugin', '2017-01-18 20:18:51.057516', '2017-01-19 02:54:57.719624', NULL, 100, 1, 0, '003L'),
(131, 0, 'ru', 'PicturePlugin', '2017-01-19 02:52:11.864784', '2017-01-19 02:54:57.756114', NULL, 103, 1, 0, '003M'),
(132, 0, 'ru', 'PicturePlugin', '2017-01-19 02:54:18.107027', '2017-01-19 02:54:57.793433', NULL, 104, 1, 0, '003N');

-- --------------------------------------------------------

--
-- Table structure for table `cms_globalpagepermission`
--

CREATE TABLE `cms_globalpagepermission` (
  `id` int(11) NOT NULL,
  `can_change` tinyint(1) NOT NULL,
  `can_add` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `can_change_advanced_settings` tinyint(1) NOT NULL,
  `can_publish` tinyint(1) NOT NULL,
  `can_change_permissions` tinyint(1) NOT NULL,
  `can_move_page` tinyint(1) NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `can_recover_page` tinyint(1) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_globalpagepermission_sites`
--

CREATE TABLE `cms_globalpagepermission_sites` (
  `id` int(11) NOT NULL,
  `globalpagepermission_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page`
--

CREATE TABLE `cms_page` (
  `id` int(11) NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `changed_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `changed_date` datetime(6) NOT NULL,
  `publication_date` datetime(6) DEFAULT NULL,
  `publication_end_date` datetime(6) DEFAULT NULL,
  `in_navigation` tinyint(1) NOT NULL,
  `soft_root` tinyint(1) NOT NULL,
  `reverse_id` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `navigation_extenders` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `login_required` tinyint(1) NOT NULL,
  `limit_visibility_in_menu` smallint(6) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL,
  `application_urls` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `application_namespace` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publisher_is_draft` tinyint(1) NOT NULL,
  `languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revision_id` int(10) UNSIGNED NOT NULL,
  `xframe_options` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `publisher_public_id` int(11) DEFAULT NULL,
  `site_id` int(11) NOT NULL,
  `depth` int(10) UNSIGNED NOT NULL,
  `numchild` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_page`
--

INSERT INTO `cms_page` (`id`, `created_by`, `changed_by`, `creation_date`, `changed_date`, `publication_date`, `publication_end_date`, `in_navigation`, `soft_root`, `reverse_id`, `navigation_extenders`, `template`, `login_required`, `limit_visibility_in_menu`, `is_home`, `application_urls`, `application_namespace`, `publisher_is_draft`, `languages`, `revision_id`, `xframe_options`, `parent_id`, `publisher_public_id`, `site_id`, `depth`, `numchild`, `path`) VALUES
(5, 'root', 'root', '2017-01-15 17:57:49.175809', '2017-01-18 17:40:41.819104', '2017-01-15 17:58:43.926873', NULL, 1, 0, NULL, '', 'template_1.html', 0, NULL, 0, '', NULL, 1, 'ru', 0, 0, NULL, 6, 1, 1, 0, '0008'),
(6, 'root', 'root', '2017-01-15 17:58:43.958590', '2017-01-18 17:40:42.136252', '2017-01-15 17:58:43.926873', NULL, 1, 0, NULL, '', 'template_1.html', 0, NULL, 0, '', NULL, 0, 'ru', 0, 0, NULL, 5, 1, 1, 0, '0009'),
(9, 'root', 'root', '2017-01-16 15:58:09.246200', '2017-01-19 02:54:57.994971', '2017-01-16 15:58:51.020664', NULL, 1, 0, NULL, '', 'template_2.html', 0, NULL, 1, '', NULL, 1, 'ru', 0, 0, NULL, 10, 1, 1, 0, '0005'),
(10, 'root', 'root', '2017-01-16 15:58:51.030556', '2017-01-19 02:54:57.878953', '2017-01-16 15:58:51.020664', NULL, 1, 0, NULL, '', 'template_2.html', 0, NULL, 1, '', NULL, 0, 'ru', 0, 0, NULL, 9, 1, 1, 0, '0006');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pagepermission`
--

CREATE TABLE `cms_pagepermission` (
  `id` int(11) NOT NULL,
  `can_change` tinyint(1) NOT NULL,
  `can_add` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  `can_change_advanced_settings` tinyint(1) NOT NULL,
  `can_publish` tinyint(1) NOT NULL,
  `can_change_permissions` tinyint(1) NOT NULL,
  `can_move_page` tinyint(1) NOT NULL,
  `can_view` tinyint(1) NOT NULL,
  `grant_on` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pageuser`
--

CREATE TABLE `cms_pageuser` (
  `user_ptr_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_pageusergroup`
--

CREATE TABLE `cms_pageusergroup` (
  `group_ptr_id` int(11) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_placeholders`
--

CREATE TABLE `cms_page_placeholders` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `placeholder_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_page_placeholders`
--

INSERT INTO `cms_page_placeholders` (`id`, `page_id`, `placeholder_id`) VALUES
(1, 5, 2),
(2, 5, 3),
(37, 5, 38),
(38, 5, 39),
(39, 5, 40),
(40, 5, 41),
(41, 5, 42),
(42, 5, 43),
(3, 6, 4),
(4, 6, 5),
(31, 6, 32),
(32, 6, 33),
(33, 6, 34),
(34, 6, 35),
(35, 6, 36),
(36, 6, 37),
(13, 9, 14),
(14, 9, 15),
(17, 9, 18),
(19, 9, 20),
(20, 9, 21),
(21, 9, 22),
(28, 9, 29),
(29, 9, 30),
(30, 9, 31),
(46, 9, 47),
(47, 9, 48),
(48, 9, 49),
(49, 9, 50),
(50, 9, 51),
(51, 9, 52),
(52, 9, 53),
(53, 9, 54),
(54, 9, 55),
(55, 9, 56),
(56, 9, 57),
(57, 9, 58),
(58, 9, 59),
(59, 9, 60),
(60, 9, 61),
(61, 9, 62),
(62, 9, 63),
(63, 9, 64),
(64, 9, 65),
(65, 9, 66),
(66, 9, 67),
(67, 9, 68),
(68, 9, 69),
(69, 9, 70),
(70, 9, 71),
(96, 9, 97),
(97, 9, 98),
(100, 9, 101),
(101, 9, 102),
(15, 10, 16),
(16, 10, 17),
(18, 10, 19),
(22, 10, 23),
(23, 10, 24),
(27, 10, 28),
(43, 10, 44),
(44, 10, 45),
(45, 10, 46),
(71, 10, 72),
(72, 10, 73),
(73, 10, 74),
(74, 10, 75),
(75, 10, 76),
(76, 10, 77),
(77, 10, 78),
(78, 10, 79),
(79, 10, 80),
(80, 10, 81),
(81, 10, 82),
(82, 10, 83),
(83, 10, 84),
(84, 10, 85),
(85, 10, 86),
(86, 10, 87),
(87, 10, 88),
(88, 10, 89),
(89, 10, 90),
(90, 10, 91),
(91, 10, 92),
(92, 10, 93),
(93, 10, 94),
(95, 10, 95),
(94, 10, 96),
(98, 10, 99),
(99, 10, 100),
(102, 10, 103),
(103, 10, 104);

-- --------------------------------------------------------

--
-- Table structure for table `cms_placeholder`
--

CREATE TABLE `cms_placeholder` (
  `id` int(11) NOT NULL,
  `slot` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_width` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_placeholder`
--

INSERT INTO `cms_placeholder` (`id`, `slot`, `default_width`) VALUES
(1, 'clipboard', NULL),
(2, 'template_1_content', NULL),
(3, 'base_content', NULL),
(4, 'template_1_content', NULL),
(5, 'base_content', NULL),
(14, 'template_1_content', NULL),
(15, 'base_content', NULL),
(16, 'template_1_content', NULL),
(17, 'base_content', NULL),
(18, 'border_png', NULL),
(19, 'border_png', NULL),
(20, 'content', NULL),
(21, 'border_png2', NULL),
(22, 'b1', NULL),
(23, 'border_png2', NULL),
(24, 'b1', NULL),
(28, 'content', NULL),
(29, 'link_slider', NULL),
(30, 'b2', NULL),
(31, 'b3', NULL),
(32, 'link_slider', NULL),
(33, 'border_png', NULL),
(34, 'border_png2', NULL),
(35, 'b1', NULL),
(36, 'b2', NULL),
(37, 'b3', NULL),
(38, 'link_slider', NULL),
(39, 'border_png', NULL),
(40, 'border_png2', NULL),
(41, 'b1', NULL),
(42, 'b2', NULL),
(43, 'b3', NULL),
(44, 'link_slider', NULL),
(45, 'b2', NULL),
(46, 'b3', NULL),
(47, 'img_previous_thumb', NULL),
(48, 'img_next_thumb', NULL),
(49, 'href_full_image_staff1', NULL),
(50, 'href_full_image_staff2', NULL),
(51, 'href_full_image_staff3', NULL),
(52, 'left-quotes', NULL),
(53, 'right-quotes3', NULL),
(54, 'img_2', NULL),
(55, 'left-quotes3', NULL),
(56, 'img_1', NULL),
(57, 'left-quotes2', NULL),
(58, 'right-quotes', NULL),
(59, 'img_3', NULL),
(60, 'img_border', NULL),
(61, 'img_t1', NULL),
(62, 'img_t2', NULL),
(63, 'img_t3', NULL),
(64, 'img_t6', NULL),
(65, 'img_t1_2', NULL),
(66, 'img_t2_2', NULL),
(67, 'img_t3_2', NULL),
(68, 'img_t4_2', NULL),
(69, 'img_t5_2', NULL),
(70, 'img_t6_2', NULL),
(71, 'img_border3', NULL),
(72, 'href_full_image_staff1', NULL),
(73, 'href_full_image_staff2', NULL),
(74, 'href_full_image_staff3', NULL),
(75, 'left-quotes', NULL),
(76, 'right-quotes3', NULL),
(77, 'img_2', NULL),
(78, 'left-quotes3', NULL),
(79, 'img_1', NULL),
(80, 'left-quotes2', NULL),
(81, 'right-quotes', NULL),
(82, 'img_3', NULL),
(83, 'img_border', NULL),
(84, 'img_t1', NULL),
(85, 'img_t2', NULL),
(86, 'img_t3', NULL),
(87, 'img_t6', NULL),
(88, 'img_t1_2', NULL),
(89, 'img_t2_2', NULL),
(90, 'img_t3_2', NULL),
(91, 'img_t4_2', NULL),
(92, 'img_t5_2', NULL),
(93, 'img_t6_2', NULL),
(94, 'img_border3', NULL),
(95, 'img_previous_thumb', NULL),
(96, 'img_next_thumb', NULL),
(97, 'img_t4', NULL),
(98, 'img_t5', NULL),
(99, 'img_t4', NULL),
(100, 'img_t5', NULL),
(101, 'img_border_masters', NULL),
(102, 'img_border_2', NULL),
(103, 'img_border_masters', NULL),
(104, 'img_border_2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_placeholderreference`
--

CREATE TABLE `cms_placeholderreference` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `placeholder_ref_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_staticplaceholder`
--

CREATE TABLE `cms_staticplaceholder` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  `creation_method` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `draft_id` int(11) DEFAULT NULL,
  `public_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_title`
--

CREATE TABLE `cms_title` (
  `id` int(11) NOT NULL,
  `language` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `has_url_overwrite` tinyint(1) NOT NULL,
  `redirect` varchar(2048) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime(6) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `publisher_is_draft` tinyint(1) NOT NULL,
  `publisher_state` smallint(6) NOT NULL,
  `page_id` int(11) NOT NULL,
  `publisher_public_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_title`
--

INSERT INTO `cms_title` (`id`, `language`, `title`, `page_title`, `menu_title`, `meta_description`, `slug`, `path`, `has_url_overwrite`, `redirect`, `creation_date`, `published`, `publisher_is_draft`, `publisher_state`, `page_id`, `publisher_public_id`) VALUES
(5, 'ru', 'Hello world', '', '', '', 'hello-world', 'hello-world', 0, '', '2017-01-15 17:57:49.630017', 1, 1, 0, 5, 6),
(6, 'ru', 'Hello world', '', '', '', 'hello-world', 'hello-world', 0, '', '2017-01-15 17:57:49.630017', 1, 0, 1, 6, 5),
(9, 'ru', 'Beauty Bar', '', 'Main', '', 'index', '', 0, '', '2017-01-16 15:58:09.524532', 1, 1, 0, 9, 10),
(10, 'ru', 'Beauty Bar', '', 'Main', '', 'index', '', 0, '', '2017-01-16 15:58:09.524532', 1, 0, 1, 10, 9);

-- --------------------------------------------------------

--
-- Table structure for table `cms_urlconfrevision`
--

CREATE TABLE `cms_urlconfrevision` (
  `id` int(11) NOT NULL,
  `revision` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_urlconfrevision`
--

INSERT INTO `cms_urlconfrevision` (`id`, `revision`) VALUES
(1, 'c0523364-a37b-4075-b0f2-bf875c3180fd');

-- --------------------------------------------------------

--
-- Table structure for table `cms_usersettings`
--

CREATE TABLE `cms_usersettings` (
  `id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `clipboard_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_usersettings`
--

INSERT INTO `cms_usersettings` (`id`, `language`, `clipboard_id`, `user_id`) VALUES
(1, 'ru', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_file_file`
--

CREATE TABLE `djangocms_file_file` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_src_id` int(11),
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_file_size` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_file_folder`
--

CREATE TABLE `djangocms_file_folder` (
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_file_size` tinyint(1) NOT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `folder_src_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_flash_flash`
--

CREATE TABLE `djangocms_flash_flash` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `file` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(6) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_googlemap_googlemap`
--

CREATE TABLE `djangocms_googlemap_googlemap` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zoom` smallint(5) UNSIGNED NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `width` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `scrollwheel` tinyint(1) NOT NULL,
  `double_click_zoom` tinyint(1) NOT NULL,
  `draggable` tinyint(1) NOT NULL,
  `keyboard_shortcuts` tinyint(1) NOT NULL,
  `pan_control` tinyint(1) NOT NULL,
  `zoom_control` tinyint(1) NOT NULL,
  `street_view_control` tinyint(1) NOT NULL,
  `style` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fullscreen_control` tinyint(1) NOT NULL,
  `map_type_control` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rotate_control` tinyint(1) NOT NULL,
  `scale_control` tinyint(1) NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_googlemap_googlemapmarker`
--

CREATE TABLE `djangocms_googlemap_googlemapmarker` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` decimal(10,6) DEFAULT NULL,
  `lng` decimal(10,6) DEFAULT NULL,
  `show_content` tinyint(1) NOT NULL,
  `info_content` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_googlemap_googlemaproute`
--

CREATE TABLE `djangocms_googlemap_googlemaproute` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `origin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `travel_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_link_link`
--

CREATE TABLE `djangocms_link_link` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `external_link` varchar(2040) COLLATE utf8_unicode_ci NOT NULL,
  `anchor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `internal_link_id` int(11) DEFAULT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `djangocms_link_link`
--

INSERT INTO `djangocms_link_link` (`cmsplugin_ptr_id`, `name`, `external_link`, `anchor`, `mailto`, `phone`, `target`, `internal_link_id`, `attributes`, `template`) VALUES
(22, 'Beauty Bar', '', '', '', '', '', 9, '{}', 'default'),
(103, 'Beauty Bar', '', '', '', '', '', 9, '{}', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_picture_picture`
--

CREATE TABLE `djangocms_picture_picture` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `link_url` varchar(2040) COLLATE utf8_unicode_ci NOT NULL,
  `alignment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_page_id` int(11) DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `picture_id` int(11),
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `caption_text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link_attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link_target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `use_automatic_scaling` tinyint(1) NOT NULL,
  `use_crop` tinyint(1) NOT NULL,
  `use_no_cropping` tinyint(1) NOT NULL,
  `use_upscale` tinyint(1) NOT NULL,
  `thumbnail_options_id` int(11),
  `external_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `djangocms_picture_picture`
--

INSERT INTO `djangocms_picture_picture` (`cmsplugin_ptr_id`, `link_url`, `alignment`, `link_page_id`, `height`, `width`, `picture_id`, `attributes`, `caption_text`, `link_attributes`, `link_target`, `use_automatic_scaling`, `use_crop`, `use_no_cropping`, `use_upscale`, `thumbnail_options_id`, `external_picture`, `template`) VALUES
(10, '', '', NULL, NULL, NULL, 3, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(11, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(13, '', '', NULL, NULL, NULL, 1, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(14, '', '', NULL, NULL, NULL, 4, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(23, '', '', NULL, NULL, NULL, 5, '{"alt": "", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(24, '', '', NULL, NULL, NULL, 6, '{"alt": "", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(26, '', '', 9, NULL, NULL, 8, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(27, '', '', 9, NULL, NULL, 9, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(28, '', '', 9, NULL, NULL, 10, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(40, '', '', NULL, NULL, NULL, 11, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(41, '', '', NULL, NULL, NULL, 12, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(42, '', '', NULL, NULL, NULL, 13, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(43, '', '', NULL, NULL, NULL, 14, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(44, '', '', NULL, NULL, NULL, 15, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(45, '', '', NULL, NULL, NULL, 12, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(46, '', '', NULL, NULL, NULL, 11, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(47, '', '', NULL, NULL, NULL, 12, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(48, '', '', NULL, NULL, NULL, 16, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(49, '', '', NULL, NULL, NULL, 1, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(50, '', '', NULL, NULL, NULL, 17, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(51, '', '', NULL, NULL, NULL, 18, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(52, '', '', NULL, NULL, NULL, 19, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(53, '', '', NULL, NULL, NULL, 20, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(54, '', '', NULL, NULL, NULL, 21, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(55, '', '', NULL, NULL, NULL, 22, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(56, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(57, '', '', NULL, NULL, NULL, 17, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(58, '', '', NULL, NULL, NULL, 18, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(59, '', '', NULL, NULL, NULL, 19, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(60, '', '', NULL, NULL, NULL, 20, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(61, '', '', NULL, NULL, NULL, 21, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(62, '', '', NULL, NULL, NULL, 22, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(96, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(97, '', '', NULL, NULL, NULL, 23, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(98, '', '', NULL, NULL, NULL, 3, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(99, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(101, '', '', NULL, NULL, NULL, 1, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(102, '', '', NULL, NULL, NULL, 4, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(104, '', '', NULL, NULL, NULL, 5, '{"alt": "", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(105, '', '', NULL, NULL, NULL, 6, '{"alt": "", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(106, '', '', 9, NULL, NULL, 8, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(107, '', '', 9, NULL, NULL, 9, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(108, '', '', 9, NULL, NULL, 10, '{}', '', '{"title": "Full Image"}', '', 1, 0, 0, 0, NULL, '', 'default'),
(109, '', '', NULL, NULL, NULL, 11, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(110, '', '', NULL, NULL, NULL, 12, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(111, '', '', NULL, NULL, NULL, 13, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(112, '', '', NULL, NULL, NULL, 14, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(113, '', '', NULL, NULL, NULL, 15, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(114, '', '', NULL, NULL, NULL, 11, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(115, '', '', NULL, NULL, NULL, 12, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(116, '', '', NULL, NULL, NULL, 16, '{"class": "img-responsive", "alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(117, '', '', NULL, NULL, NULL, 1, '{"alt": ""}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(118, '', '', NULL, NULL, NULL, 17, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(119, '', '', NULL, NULL, NULL, 18, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(120, '', '', NULL, NULL, NULL, 19, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(121, '', '', NULL, NULL, NULL, 22, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(122, '', '', NULL, NULL, NULL, 17, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(123, '', '', NULL, NULL, NULL, 18, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(124, '', '', NULL, NULL, NULL, 19, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(125, '', '', NULL, NULL, NULL, 20, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(126, '', '', NULL, NULL, NULL, 21, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(127, '', '', NULL, NULL, NULL, 22, '{"class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(128, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(129, '', '', NULL, NULL, NULL, 20, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(130, '', '', NULL, NULL, NULL, 21, '{"alt": "img25", "class": "img-responsive"}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(131, '', '', NULL, NULL, NULL, 1, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default'),
(132, '', '', NULL, NULL, NULL, 23, '{}', '', '{}', '', 1, 0, 0, 0, NULL, '', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_snippet_snippet`
--

CREATE TABLE `djangocms_snippet_snippet` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_snippet_snippetptr`
--

CREATE TABLE `djangocms_snippet_snippetptr` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `snippet_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_teaser_teaser`
--

CREATE TABLE `djangocms_teaser_teaser` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `page_link_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `djangocms_teaser_teaser`
--

INSERT INTO `djangocms_teaser_teaser` (`cmsplugin_ptr_id`, `title`, `image`, `url`, `description`, `page_link_id`) VALUES
(2, 'Hello world', '', '', '', NULL),
(6, 'Hello world', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_text_ckeditor_text`
--

CREATE TABLE `djangocms_text_ckeditor_text` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `djangocms_text_ckeditor_text`
--

INSERT INTO `djangocms_text_ckeditor_text` (`cmsplugin_ptr_id`, `body`) VALUES
(5, '\n<title></title>\n&lt;meta name="viewport" content="width=device-width, initial-scale=1"&gt;&lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8"/&gt;&lt;meta name="keywords" content="New Hair Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, \nSmartphone Compatible web template, free web designs for Nokia, Samsung, LG,sonyEricsson, Motorola web design"/&gt;&lt;script type="application/x-javascript"&gt; addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } &lt;/script&gt;\n&lt;link href="css/bootstrap.css" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Open+Sans:700,700italic,800,300,300italic,400italic,400,600,600italic" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="css/style.css" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="css/slider.css" rel="stylesheet"/&gt;&lt;script src="js/jquery.min.js"&gt; &lt;/script&gt;&lt;script type="text/javascript" src="js/move-top.js"&gt;&lt;/script&gt;&lt;script type="text/javascript" src="js/easing.js"&gt;&lt;/script&gt;&lt;script type="text/javascript"&gt;\n			jQuery(document).ready(function($) {\n				$(".scroll").click(function(event){		\n					event.preventDefault();\n					$(\'html,body\').animate({scrollTop:$(this.hash).offset().top},900);\n				});\n			});\n&lt;/script&gt;\n<div class="top_banner">\n<div class="svg-wrap"><svg width="64" height="64"> <path id="arrow-left" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z"></path> </svg> <svg width="64" height="64"> <path id="arrow-right" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z"></path> </svg></div>\n\n<div data-wow-duration=".8s" class="menu wow fadeInDown" data-wow-delay=".2s">\n<div class="socialCircle-container">\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-center closed"> </div>\n</div>\n</div>\n&lt;script src="js/socialCircle.js"&gt;&lt;/script&gt;&lt;script type="text/javascript"&gt;\n		$( ".socialCircle-center" ).socialCircle({\n			rotate: 0,\n			radius:140,\n			circleSize: 2,\n			speed:500\n		});\n		&lt;/script&gt;\n\n<div data-wow-duration=".8s" class="logo wow fadeInRight" data-wow-delay=".5s">\n<h1><a href="index.html">H <span>&amp; </span> B</a></h1>\n</div>\n\n<div id="home" class="sleekslider">\n<div class="slide active bg-1">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".2s">Hair and Beauty Salon</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-2">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">We care for your Hair</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-3">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">New season Hair syles</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-4">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">Express your Style in length!</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-5">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">Welcome to Hair salon</p>\n</div>\n</div>\n</div>\n\n\n<nav class="nav-split"><a class="prev" href=""> <span class="icon-wrap"><svg width="22" class="icon" height="22"><use href="#arrow-left"></use></svg></span> </a>\n\n<div>\n<h3><a class="prev" href="">test</a></h3>\n<a class="prev" href=""><img alt="Previous thumb"> </a></div>\n<a class="prev" href=""> </a> <a class="next" href=""> <span class="icon-wrap"><svg width="22" class="icon" height="22"><use href="#arrow-right"></use></svg></span> </a>\n\n<div>\n<h3><a class="next" href="">test</a></h3>\n<a class="next" href=""><img alt="Next thumb"> </a></div>\n<a class="next" href=""> </a></nav>\n\n\n<nav class="pagination"> </nav>\n\n\n<nav class="tabs">\n<div class="tab-container">\n<ul>\n	<li class="current"><a href="#"><span>01</span> Slide</a></li>\n	<li><a href="#"><span>02</span> Slide</a></li>\n	<li><a href="#"><span>03</span> Slide</a></li>\n	<li><a href="#"><span>04</span> Slide</a></li>\n	<li><a href="#"><span>05</span> Slide</a></li>\n</ul>\n</div>\n</nav>\n</div>\n&lt;script type="text/javascript" src="js/sleekslider.min.js"&gt;&lt;/script&gt;&lt;script type="text/javascript" src="js/app.js"&gt;&lt;/script&gt;</div>\n\n<div id="promotion" class="welcome">\n<div class="container">\n<div class="sub-head">\n<h2 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Promotions</h2>\n<img src="images/border.png" alt=""></div>\n\n<div class="welcome-info">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n\n<div class="welcome-row">\n<div data-wow-duration=".8s" class="col-md-6 welcome-grid wow fadeInLeft" data-wow-delay=".3s">\n<div class="welcome-left">\n<div class="welcome-grids">\n<div class="promo-info">\n<h5 class="promo-desc">40%</h5>\n\n<p class="promo-bg2">Discount</p>\n</div>\n</div>\n</div>\n\n<div class="promo-box welcome-mdl">\n<div class="promo-box-title title-with-divider">\n<h3>Only until March 30 Discount</h3>\n</div>\n\n<p>Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accum</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div data-wow-duration=".8s" class="col-md-6 welcome-grid wow fadeInRight" data-wow-delay=".3s">\n<div class="welcome-left">\n<div class="welcome-grids">\n<div class="promo-info">\n<h5 class="promo-desc">30%</h5>\n\n<p class="promo-bg2">Discount</p>\n</div>\n</div>\n</div>\n\n<div class="promo-box welcome-mdl">\n<div class="promo-box-title title-with-divider">\n<h3>Only until Aug 30 Discount</h3>\n</div>\n\n<p>Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accum</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n</div>\n\n\n<div id="service" class="trainee-section">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Services</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="trainee-grids">\n<div data-wow-duration=".8s" class="col-md-4 trainee-grid wow fadeInLeft" data-wow-delay=".4s"><a href="index.html"><img src="images/b1.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Hair Style</h4>\n\n<div class="date">&lt;lable&gt;30&lt;/lable&gt; <span>Mar</span></div>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div style="" class="col-md-4 trainee-grid wow fadeInUp animated animated animated" data-wow-delay="0.4s"><a href="index.html"><img src="images/b2.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Makeup</h4>\n\n<div class="date">&lt;lable&gt;30&lt;/lable&gt; <span>Aug</span></div>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div data-wow-duration=".8s" class="col-md-4 trainee-grid lost wow fadeInRight" data-wow-delay=".4s"><a href="index.html"><img src="images/b3.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Manicure</h4>\n\n<div class="date">&lt;lable&gt;15&lt;/lable&gt; <span>Nov</span></div>\n<a href="index.html"> </a>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div id="staff" class="reviews">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Staff</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="staff-section">\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid  animated wow fadeInLeft" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff1.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Jennifer</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid animated wow fadeInDown" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff2.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Riya John</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid animated wow fadeInUp" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff3.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Jessica</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div id="review" class="reviews">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title review wow bounceIn" data-wow-delay=".2s">Our Reviews</h3>\n<img src="images/border2.png" alt="">\n<h4 data-wow-duration=".8s" class="text welcome-grid wow flash" data-wow-delay=".4s">Giving you a fabulous haircut with acceptable service</h4>\n</div>\n\n<div class="test-monials">\n<div class="sreen-gallery-cursual"></div>\n</div>\n</div>\n</div>\n&lt;link href="css/owl.carousel.css" rel="stylesheet"/&gt;&lt;script src="js/owl.carousel.js"&gt;&lt;/script&gt;&lt;script&gt;\n							    $(document).ready(function() {\n							      $("#owl-demo").owlCarousel({\n							        items : 1,\n							        lazyLoad : true,\n							        autoPlay : true,\n							        navigation : false,\n							        navigationText :  false,\n							        pagination : true,\n							      });\n							    });\n							    &lt;/script&gt;\n<div id="owl-demo" class="owl-carousel">\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt=""> Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/2.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Victoria,</h5>\n</div>\n</div>\n\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt="">Polite sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/1.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Sovika,</h5>\n</div>\n</div>\n\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt=""> Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/3.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Leena,</h5>\n</div>\n</div>\n</div>\n\n\n<div id="gallery" class="portfolio">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Gallery</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="portfolio-top wow fadeInDown animated" data-wow-delay=".5s">\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal1"><img src="images/t1.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal1">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal1"> </a></figure>\n</div>\n\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal2"><img src="images/t2.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal2">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal2"> </a></figure>\n</div>\n\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal3"><img src="images/t3.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal3">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal3"> </a></figure>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="portfolio-top two wow fadeInUp animated" data-wow-delay=".5s">\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal4"><img src="images/t4.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal4">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal4"> </a></figure>\n</div>\n\n<div class="col-md-4 grid grid-wi slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal5"><img src="images/t5.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal5">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal5"> </a></figure>\n</div>\n\n<div class="col-md-4 grid grid-wi slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal6"><img src="images/t6.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal6">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal6"> </a></figure>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div tabindex="-1" id="portfolioModal1" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t1.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal2" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t2.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal3" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t3.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal4" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t4.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal5" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t5.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal6" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t6.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n\n<div id="contact" class="section-contact">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Contact Us</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="contact-main">\n<div data-wow-duration="1s" class="col-md-6 contact-grid wow fadeInUp" data-wow-delay=".3s">\n<form><span class="input input--haruki"><input type="text" id="input-1" required class="input__field input__field--haruki"> <label class="input__label input__label--haruki" for="input-1"> <span class="input__label-content input__label-content--haruki">Your Name</span> </label> </span> <span class="input input--haruki"> <input type="text" id="input-3" required class="input__field input__field--haruki"> <label class="input__label input__label--haruki" for="input-3"> <span class="input__label-content input__label-content--haruki">Your Email</span> </label> </span> <span class="input input--haruki lost"><textarea type="textarea" id="msg" required="" class="textarea__field input__field--haruki"></textarea> <label class="input__label input__label--haruki" for="textarea"> <span class="input__label-content input__label-content--haruki">Your Message</span> </label> </span>\n\n<div data-wow-duration="1s" class="send wow shake" data-wow-delay=".5s"><input type="submit" value="Send"></div>\n</form>\n</div>\n\n<div data-wow-duration="1s" class="col-md-6 contact-in wow fadeInUp" data-wow-delay=".5s">\n<h4 class="info">Our Info</h4>\n\n<p class="para1">Lorem ipsum dolor sit amet. Ut enim ad minim veniam.Sunt in culpa qui officia deserunt mollit anim id est laborum.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>\n\n<div class="more-address">\n<address><strong class="one">Twitter, Inc.</strong><br>\n795 Folsom Ave, Suite 600<br>\nSan Francisco, CA 94107<br>\n<abbr title="Phone">Phone :</abbr> (123) 456-7890</address>\n\n<address><strong class="one">Full Name</strong><br>\n<a href="mailto:info@example.com">mail@example.com</a></address>\n</div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n\n<div data-wow-duration=".8s" class="map wow fadeInDown" data-wow-delay=".5s">&lt;iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1675115.8258740564!2d-98.4671417929578!3d34.91371150021706!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1434956093308"&gt;&lt;/iframe&gt;</div>\n</div>\n</div>\n\n\n<div class="footer">\n<div class="container">\n<div class="f-grids">\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<h3>Recent <span class="opening">Posts</span></h3>\n\n<h5><a href="#">The do’s and don’ts of running for fitness</a></h5>\n\n<p>Sed rhoncus nulla turpis, vitae rutrum velit iaculis et. Curabitur vestibulum, erat non im</p>\n\n<p class="month">April 1,2016 , By <a href="#"> Robert Louise</a></p>\n\n<h5><a href="#">The do’s and don’ts of running for fitness</a></h5>\n\n<p>Sed rhoncus nulla turpis, vitae rutrum velit iaculis et. Curabitur vestibulum, erat non im</p>\n\n<p class="month">April 1,2016 , By <a href="#">Robert Louise</a></p>\n</div>\n\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<div class="opening_hours">\n<h3>Opening <span class="opening">Hours</span></h3>\n\n<ul class="times">\n	<li><span class="week">Monday</span>\n\n	<div class="hours">9:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Tuesday</span>\n	<div class="hours">10:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Wednesday</span>\n	<div class="hours">9:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Thursday</span>\n	<div class="hours">9:00am to10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Friday</span>\n	<div class="hours">9:00am to 11:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Saturday</span>\n	<div class="hours">9:00am to 2:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Sunday</span>\n	<div class="hours">Closed</div>\n\n	<div class="clearfix"> </div>\n	</li>\n</ul>\n</div>\n</div>\n\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<h3>Contact <span class="opening">Info</span></h3>\n\n<ul class="address">\n	<li>123, new street, 129907 New York</li>\n	<li>023 456 23456</li>\n	<li><a class="mail" href="mailto:info@example.com">123 int@example.com</a></li>\n</ul>\n\n<div class="support">\n<form class="lost"><input type="text" required> <input type="submit" class="botton" value="SUBMIT">\n<p>Lorem ipsum dolor sit amet conse aliqua. Ut enim ad minim veniam Lorem ctetur adipisicing .</p>\n</form>\n</div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="copy">\n<p data-wow-duration="1s" class="wow fadeInUp" data-wow-delay=".3s">© 2016 H &amp; B. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>\n</div>\n</div>\n</div>\n&lt;script type="text/javascript"&gt;\n									$(document).ready(function() {\n										/*\n										var defaults = {\n								  			containerID: \'toTop\', // fading element id\n											containerHoverID: \'toTopHover\', // fading element hover id\n											scrollSpeed:1200,\n											easingType: \'linear\' \n								 		};\n										*/\n										\n										$().UItoTop({ easingType: \'easeOutQuart\' });\n										\n									});\n								&lt;/script&gt;&lt;script src="js/bootstrap.js"&gt;&lt;/script&gt;\n&lt;link href="css/animate.min.css" rel="stylesheet"/&gt;&lt;script src="js/wow.min.js"&gt;&lt;/script&gt;&lt;script&gt;\n new WOW().init();\n&lt;/script&gt;');
INSERT INTO `djangocms_text_ckeditor_text` (`cmsplugin_ptr_id`, `body`) VALUES
(7, '\n<title></title>\n&lt;meta name="viewport" content="width=device-width, initial-scale=1"&gt;&lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8"/&gt;&lt;meta name="keywords" content="New Hair Style Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, \nSmartphone Compatible web template, free web designs for Nokia, Samsung, LG,sonyEricsson, Motorola web design"/&gt;&lt;script type="application/x-javascript"&gt; addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } &lt;/script&gt;\n&lt;link href="css/bootstrap.css" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Open+Sans:700,700italic,800,300,300italic,400italic,400,600,600italic" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="css/style.css" rel="stylesheet" type="text/css"/&gt;\n&lt;link href="css/slider.css" rel="stylesheet"/&gt;&lt;script src="js/jquery.min.js"&gt; &lt;/script&gt;&lt;script type="text/javascript" src="js/move-top.js"&gt;&lt;/script&gt;&lt;script type="text/javascript" src="js/easing.js"&gt;&lt;/script&gt;&lt;script type="text/javascript"&gt;\n			jQuery(document).ready(function($) {\n				$(".scroll").click(function(event){		\n					event.preventDefault();\n					$(\'html,body\').animate({scrollTop:$(this.hash).offset().top},900);\n				});\n			});\n&lt;/script&gt;\n<div class="top_banner">\n<div class="svg-wrap"><svg width="64" height="64"> <path id="arrow-left" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z"></path> </svg> <svg width="64" height="64"> <path id="arrow-right" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z"></path> </svg></div>\n\n<div data-wow-duration=".8s" class="menu wow fadeInDown" data-wow-delay=".2s">\n<div class="socialCircle-container">\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-item"> </div>\n\n<div class="socialCircle-center closed"> </div>\n</div>\n</div>\n&lt;script src="js/socialCircle.js"&gt;&lt;/script&gt;&lt;script type="text/javascript"&gt;\n		$( ".socialCircle-center" ).socialCircle({\n			rotate: 0,\n			radius:140,\n			circleSize: 2,\n			speed:500\n		});\n		&lt;/script&gt;\n\n<div data-wow-duration=".8s" class="logo wow fadeInRight" data-wow-delay=".5s">\n<h1><a href="index.html">H <span>&amp; </span> B</a></h1>\n</div>\n\n<div id="home" class="sleekslider">\n<div class="slide active bg-1">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".2s">Hair and Beauty Salon</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-2">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">We care for your Hair</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-3">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">New season Hair syles</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-4">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">Express your Style in length!</p>\n</div>\n</div>\n</div>\n\n<div class="slide bg-5">\n<div class="slide-container">\n<div class="slide-content">\n<p data-wow-duration=".8s" class="wow fadeInLeft" data-wow-delay=".4s">Welcome to Hair salon</p>\n</div>\n</div>\n</div>\n\n\n<nav class="nav-split"><a class="prev" href=""> <span class="icon-wrap"><svg width="22" class="icon" height="22"><use href="#arrow-left"></use></svg></span> </a>\n\n<div>\n<h3><a class="prev" href="">test</a></h3>\n<a class="prev" href=""><img alt="Previous thumb"> </a></div>\n<a class="prev" href=""> </a> <a class="next" href=""> <span class="icon-wrap"><svg width="22" class="icon" height="22"><use href="#arrow-right"></use></svg></span> </a>\n\n<div>\n<h3><a class="next" href="">test</a></h3>\n<a class="next" href=""><img alt="Next thumb"> </a></div>\n<a class="next" href=""> </a></nav>\n\n\n<nav class="pagination"> </nav>\n\n\n<nav class="tabs">\n<div class="tab-container">\n<ul>\n	<li class="current"><a href="#"><span>01</span> Slide</a></li>\n	<li><a href="#"><span>02</span> Slide</a></li>\n	<li><a href="#"><span>03</span> Slide</a></li>\n	<li><a href="#"><span>04</span> Slide</a></li>\n	<li><a href="#"><span>05</span> Slide</a></li>\n</ul>\n</div>\n</nav>\n</div>\n&lt;script type="text/javascript" src="js/sleekslider.min.js"&gt;&lt;/script&gt;&lt;script type="text/javascript" src="js/app.js"&gt;&lt;/script&gt;</div>\n\n<div id="promotion" class="welcome">\n<div class="container">\n<div class="sub-head">\n<h2 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Promotions</h2>\n<img src="images/border.png" alt=""></div>\n\n<div class="welcome-info">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n\n<div class="welcome-row">\n<div data-wow-duration=".8s" class="col-md-6 welcome-grid wow fadeInLeft" data-wow-delay=".3s">\n<div class="welcome-left">\n<div class="welcome-grids">\n<div class="promo-info">\n<h5 class="promo-desc">40%</h5>\n\n<p class="promo-bg2">Discount</p>\n</div>\n</div>\n</div>\n\n<div class="promo-box welcome-mdl">\n<div class="promo-box-title title-with-divider">\n<h3>Only until March 30 Discount</h3>\n</div>\n\n<p>Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accum</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div data-wow-duration=".8s" class="col-md-6 welcome-grid wow fadeInRight" data-wow-delay=".3s">\n<div class="welcome-left">\n<div class="welcome-grids">\n<div class="promo-info">\n<h5 class="promo-desc">30%</h5>\n\n<p class="promo-bg2">Discount</p>\n</div>\n</div>\n</div>\n\n<div class="promo-box welcome-mdl">\n<div class="promo-box-title title-with-divider">\n<h3>Only until Aug 30 Discount</h3>\n</div>\n\n<p>Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accum</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n</div>\n\n\n<div id="service" class="trainee-section">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Services</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="trainee-grids">\n<div data-wow-duration=".8s" class="col-md-4 trainee-grid wow fadeInLeft" data-wow-delay=".4s"><a href="index.html"><img src="images/b1.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Hair Style</h4>\n\n<div class="date">&lt;lable&gt;30&lt;/lable&gt; <span>Mar</span></div>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div style="" class="col-md-4 trainee-grid wow fadeInUp animated animated animated" data-wow-delay="0.4s"><a href="index.html"><img src="images/b2.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Makeup</h4>\n\n<div class="date">&lt;lable&gt;30&lt;/lable&gt; <span>Aug</span></div>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div data-wow-duration=".8s" class="col-md-4 trainee-grid lost wow fadeInRight" data-wow-delay=".4s"><a href="index.html"><img src="images/b3.jpg" alt="" class="img-responsive"></a>\n\n<h4 class="title">Manicure</h4>\n\n<div class="date">&lt;lable&gt;15&lt;/lable&gt; <span>Nov</span></div>\n<a href="index.html"> </a>\n\n<h4><a href="index.html">Lorem ipsum dolor sit amet,</a></h4>\n\n<h5><a href="index.html">consectetuer adipiscing.</a></h5>\n\n<p>Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem ipsum dolor sit amet,…</p>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div id="staff" class="reviews">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Staff</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="staff-section">\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid  animated wow fadeInLeft" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff1.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Jennifer</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid animated wow fadeInDown" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff2.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Riya John</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div data-wow-duration="1000ms" class="col-md-4 staff-grid animated wow fadeInUp" data-wow-delay="500ms">\n<div class="view fifth-effect"><a title="Full Image" href="index.html"><img src="images/staff3.jpg"></a>\n\n<div class="mask"> </div>\n</div>\n\n<h4>Jessica</h4>\n\n<p>Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>\n\n<ul class="social-icons">\n	<li> </li>\n	<li> </li>\n	<li> </li>\n	<li> </li>\n</ul>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div id="review" class="reviews">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title review wow bounceIn" data-wow-delay=".2s">Our Reviews</h3>\n<img src="images/border2.png" alt="">\n<h4 data-wow-duration=".8s" class="text welcome-grid wow flash" data-wow-delay=".4s">Giving you a fabulous haircut with acceptable service</h4>\n</div>\n\n<div class="test-monials">\n<div class="sreen-gallery-cursual"></div>\n</div>\n</div>\n</div>\n&lt;link href="css/owl.carousel.css" rel="stylesheet"/&gt;&lt;script src="js/owl.carousel.js"&gt;&lt;/script&gt;&lt;script&gt;\n							    $(document).ready(function() {\n							      $("#owl-demo").owlCarousel({\n							        items : 1,\n							        lazyLoad : true,\n							        autoPlay : true,\n							        navigation : false,\n							        navigationText :  false,\n							        pagination : true,\n							      });\n							    });\n							    &lt;/script&gt;\n<div id="owl-demo" class="owl-carousel">\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt=""> Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/2.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Victoria,</h5>\n</div>\n</div>\n\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt="">Polite sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/1.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Sovika,</h5>\n</div>\n</div>\n\n<div class="item-owl">\n<div class="test-review">\n<p data-wow-duration=".8s" class="wow fadeInUp" data-wow-delay=".4s"><img src="images/left-quotes.png" alt=""> Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum stet clita kasd gubergren <img src="images/right-quotes.png" alt=""></p>\n<img src="images/3.jpg" alt="" class="img-responsive">\n<h5 data-wow-duration=".8s" class="wow bounceIn" data-wow-delay=".2s">Dennis Leena,</h5>\n</div>\n</div>\n</div>\n\n\n<div id="gallery" class="portfolio">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Our Gallery</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="portfolio-top wow fadeInDown animated" data-wow-delay=".5s">\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal1"><img src="images/t1.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal1">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal1"> </a></figure>\n</div>\n\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal2"><img src="images/t2.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal2">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal2"> </a></figure>\n</div>\n\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal3"><img src="images/t3.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal3">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal3"> </a></figure>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="portfolio-top two wow fadeInUp animated" data-wow-delay=".5s">\n<div class="col-md-4 grid slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal4"><img src="images/t4.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal4">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal4"> </a></figure>\n</div>\n\n<div class="col-md-4 grid grid-wi slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal5"><img src="images/t5.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal5">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal5"> </a></figure>\n</div>\n\n<div class="col-md-4 grid grid-wi slideanim">\n<figure class="effect-layla"><a data-toggle="modal" href="#portfolioModal6"><img src="images/t6.jpg" alt="img25" class="img-responsive"> </a>\n\n<figcaption>\n<h4><a data-toggle="modal" href="#portfolioModal6">New <span>Hair</span> Style</a></h4>\n</figcaption>\n<a data-toggle="modal" href="#portfolioModal6"> </a></figure>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n</div>\n</div>\n\n\n<div tabindex="-1" id="portfolioModal1" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t1.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal2" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t2.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal3" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t3.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal4" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t4.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal5" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t5.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n<div tabindex="-1" id="portfolioModal6" class="portfolio-modal modal fade slideanim">\n<div class="modal-content">\n<div class="close-modal" data-dismiss="modal">\n<div class="lr">\n<div class="rl"> </div>\n</div>\n</div>\n\n<div class="container">\n<div class="col-lg-8 col-lg-offset-2">\n<div class="modal-body">\n<h3>New <span>Hair</span> Style</h3>\n<img src="images/t6.jpg" alt="" class="img-responsive">\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\n</div>\n</div>\n</div>\n</div>\n</div>\n\n\n<div id="contact" class="section-contact">\n<div class="container">\n<div class="sub-head">\n<h3 data-wow-duration=".8s" class="title wow bounceIn" data-wow-delay=".2s">Contact Us</h3>\n<img src="images/border.png" alt="">\n<p data-wow-duration=".8s" class="wel-text wow fadeInDown" data-wow-delay=".4s">Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus omnis optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>\n</div>\n\n<div class="contact-main">\n<div data-wow-duration="1s" class="col-md-6 contact-grid wow fadeInUp" data-wow-delay=".3s">\n<form><span class="input input--haruki"><input type="text" id="input-1" required class="input__field input__field--haruki"> <label class="input__label input__label--haruki" for="input-1"> <span class="input__label-content input__label-content--haruki">Your Name</span> </label> </span> <span class="input input--haruki"> <input type="text" id="input-3" required class="input__field input__field--haruki"> <label class="input__label input__label--haruki" for="input-3"> <span class="input__label-content input__label-content--haruki">Your Email</span> </label> </span> <span class="input input--haruki lost"><textarea type="textarea" id="msg" required="" class="textarea__field input__field--haruki"></textarea> <label class="input__label input__label--haruki" for="textarea"> <span class="input__label-content input__label-content--haruki">Your Message</span> </label> </span>\n\n<div data-wow-duration="1s" class="send wow shake" data-wow-delay=".5s"><input type="submit" value="Send"></div>\n</form>\n</div>\n\n<div data-wow-duration="1s" class="col-md-6 contact-in wow fadeInUp" data-wow-delay=".5s">\n<h4 class="info">Our Info</h4>\n\n<p class="para1">Lorem ipsum dolor sit amet. Ut enim ad minim veniam.Sunt in culpa qui officia deserunt mollit anim id est laborum.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature.</p>\n\n<div class="more-address">\n<address><strong class="one">Twitter, Inc.</strong><br>\n795 Folsom Ave, Suite 600<br>\nSan Francisco, CA 94107<br>\n<abbr title="Phone">Phone :</abbr> (123) 456-7890</address>\n\n<address><strong class="one">Full Name</strong><br>\n<a href="mailto:info@example.com">mail@example.com</a></address>\n</div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n\n<div data-wow-duration=".8s" class="map wow fadeInDown" data-wow-delay=".5s">&lt;iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1675115.8258740564!2d-98.4671417929578!3d34.91371150021706!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1434956093308"&gt;&lt;/iframe&gt;</div>\n</div>\n</div>\n\n\n<div class="footer">\n<div class="container">\n<div class="f-grids">\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<h3>Recent <span class="opening">Posts</span></h3>\n\n<h5><a href="#">The do’s and don’ts of running for fitness</a></h5>\n\n<p>Sed rhoncus nulla turpis, vitae rutrum velit iaculis et. Curabitur vestibulum, erat non im</p>\n\n<p class="month">April 1,2016 , By <a href="#"> Robert Louise</a></p>\n\n<h5><a href="#">The do’s and don’ts of running for fitness</a></h5>\n\n<p>Sed rhoncus nulla turpis, vitae rutrum velit iaculis et. Curabitur vestibulum, erat non im</p>\n\n<p class="month">April 1,2016 , By <a href="#">Robert Louise</a></p>\n</div>\n\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<div class="opening_hours">\n<h3>Opening <span class="opening">Hours</span></h3>\n\n<ul class="times">\n	<li><span class="week">Monday</span>\n\n	<div class="hours">9:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Tuesday</span>\n	<div class="hours">10:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Wednesday</span>\n	<div class="hours">9:00am to 10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Thursday</span>\n	<div class="hours">9:00am to10:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Friday</span>\n	<div class="hours">9:00am to 11:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Saturday</span>\n	<div class="hours">9:00am to 2:00pm</div>\n\n	<div class="clearfix"> </div>\n	</li>\n	<li><span class="week">Sunday</span>\n	<div class="hours">Closed</div>\n\n	<div class="clearfix"> </div>\n	</li>\n</ul>\n</div>\n</div>\n\n<div data-wow-duration="1s" class="col-md-4 footer-grid wow fadeInDown" data-wow-delay=".3s">\n<h3>Contact <span class="opening">Info</span></h3>\n\n<ul class="address">\n	<li>123, new street, 129907 New York</li>\n	<li>023 456 23456</li>\n	<li><a class="mail" href="mailto:info@example.com">123 int@example.com</a></li>\n</ul>\n\n<div class="support">\n<form class="lost"><input type="text" required> <input type="submit" class="botton" value="SUBMIT">\n<p>Lorem ipsum dolor sit amet conse aliqua. Ut enim ad minim veniam Lorem ctetur adipisicing .</p>\n</form>\n</div>\n</div>\n\n<div class="clearfix"> </div>\n</div>\n\n<div class="copy">\n<p data-wow-duration="1s" class="wow fadeInUp" data-wow-delay=".3s">© 2016 H &amp; B. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>\n</div>\n</div>\n</div>\n&lt;script type="text/javascript"&gt;\n									$(document).ready(function() {\n										/*\n										var defaults = {\n								  			containerID: \'toTop\', // fading element id\n											containerHoverID: \'toTopHover\', // fading element hover id\n											scrollSpeed:1200,\n											easingType: \'linear\' \n								 		};\n										*/\n										\n										$().UItoTop({ easingType: \'easeOutQuart\' });\n										\n									});\n								&lt;/script&gt;&lt;script src="js/bootstrap.js"&gt;&lt;/script&gt;\n&lt;link href="css/animate.min.css" rel="stylesheet"/&gt;&lt;script src="js/wow.min.js"&gt;&lt;/script&gt;&lt;script&gt;\n new WOW().init();\n&lt;/script&gt;');

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_twitter_twitterrecententries`
--

CREATE TABLE `djangocms_twitter_twitterrecententries` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_user` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `count` smallint(5) UNSIGNED NOT NULL,
  `link_hint` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_id` varchar(75) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_twitter_twittersearch`
--

CREATE TABLE `djangocms_twitter_twittersearch` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `title` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `query` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `count` smallint(5) UNSIGNED NOT NULL,
  `twitter_id` varchar(75) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_video_videoplayer`
--

CREATE TABLE `djangocms_video_videoplayer` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `embed_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `poster_id` int(11),
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_video_videosource`
--

CREATE TABLE `djangocms_video_videosource` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `text_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `source_file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djangocms_video_videotrack`
--

CREATE TABLE `djangocms_video_videotrack` (
  `cmsplugin_ptr_id` int(11) NOT NULL,
  `kind` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `srclang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `src_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2017-01-14 17:25:27.062538', '1', 'Главное меню', 1, 'Добавлено.', 2, 1),
(2, '2017-01-14 17:25:48.637780', '1', 'Главное меню', 2, 'Изменен xframe_options.', 2, 1),
(3, '2017-01-14 18:09:33.109011', '1', 'Главное меню', 2, 'Изменен title.', 2, 1),
(4, '2017-01-14 18:10:31.415456', '3', 'Первая страница', 1, 'Добавлено.', 2, 1),
(5, '2017-01-14 19:12:09.777362', '2', 'beauty-salon-kate', 1, 'Добавлено.', 21, 1),
(6, '2017-01-14 19:12:18.493509', '2', 'beauty-salon-kate', 2, 'Ни одно поле не изменено.', 21, 1),
(7, '2017-01-14 19:16:42.936132', '1', 'Главное меню', 2, 'Ни одно поле не изменено.', 2, 1),
(8, '2017-01-14 19:17:14.654998', '3', 'Салон красоты "Beauty bar"', 2, '', 2, 1),
(9, '2017-01-15 17:57:49.755112', '5', 'Hello world', 1, '', 2, 1),
(10, '2017-01-15 17:58:16.480427', '5', 'Hello world', 2, 'Изменен template и xframe_options.', 2, 1),
(11, '2017-01-15 17:58:44.595607', '5', 'Hello world', 2, '', 2, 1),
(12, '2017-01-15 18:00:36.746731', '5', 'Hello world', 2, '', 2, 1),
(13, '2017-01-15 18:04:17.198083', '7', 'Главная', 1, '', 2, 1),
(14, '2017-01-15 18:04:53.516355', '7', 'Главная', 2, 'Изменен template и xframe_options.', 2, 1),
(15, '2017-01-15 18:12:40.851647', '7', 'main', 2, '', 2, 1),
(16, '2017-01-15 18:16:33.865063', '1', '1', 3, '', 8, 1),
(17, '2017-01-15 18:18:46.752162', '5', 'Hello world', 2, '', 2, 1),
(18, '2017-01-16 15:58:09.620469', '9', '234', 1, '', 2, 1),
(19, '2017-01-16 15:58:33.033985', '9', '234', 2, 'Изменен template и xframe_options.', 2, 1),
(20, '2017-01-16 15:58:51.676561', '9', 'Hello world2', 2, '', 2, 1),
(21, '2017-01-16 16:01:47.006235', '9', 'Hello world2', 2, '', 2, 1),
(22, '2017-01-16 16:09:33.708933', '8', '8', 3, '', 8, 1),
(23, '2017-01-16 16:17:02.797013', '9', 'Hello world2', 2, '', 2, 1),
(24, '2017-01-16 17:19:21.561887', '9', '234', 2, 'Изменен xframe_options.', 2, 1),
(25, '2017-01-16 17:38:38.758774', '9', 'Hello world2', 2, '', 2, 1),
(26, '2017-01-17 18:15:36.046680', '1', 'clipboard', 3, '', 1, 1),
(27, '2017-01-18 17:38:45.994349', '1', 'Главное меню', 3, '', 2, 1),
(28, '2017-01-18 17:38:54.135136', '3', 'Первая страница', 3, '', 2, 1),
(30, '2017-01-18 17:39:25.559069', '7', 'Главная', 3, '', 2, 1),
(32, '2017-01-18 17:40:22.772106', '9', '234', 2, 'Изменен title, slug и menu_title.', 2, 1),
(33, '2017-01-18 17:44:08.382878', '2', 'beauty-salon-kate', 3, '', 21, 1),
(34, '2017-01-18 17:44:25.956191', '1', 'beauty-salon-kate', 2, 'Изменен domain и name.', 21, 1),
(35, '2017-01-18 17:44:44.626409', '1', 'beauty-salon-kate.ru', 2, 'Изменен domain и name.', 21, 1),
(36, '2017-01-18 19:04:41.050959', '1', 'clipboard', 3, '', 1, 1),
(37, '2017-01-18 19:05:11.107466', '1', 'clipboard', 3, '', 1, 1),
(38, '2017-01-18 19:06:12.415031', '9', 'Beauty Bar', 2, '', 2, 1),
(39, '2017-01-18 19:31:52.367842', '1', 'clipboard', 3, '', 1, 1),
(40, '2017-01-18 20:29:43.243801', '9', 'Beauty Bar', 2, '', 2, 1),
(41, '2017-01-19 02:54:58.056238', '9', 'Beauty Bar', 2, '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(20, 'admin', 'logentry'),
(16, 'auth', 'group'),
(15, 'auth', 'permission'),
(17, 'auth', 'user'),
(12, 'cms', 'aliaspluginmodel'),
(8, 'cms', 'cmsplugin'),
(4, 'cms', 'globalpagepermission'),
(2, 'cms', 'page'),
(5, 'cms', 'pagepermission'),
(6, 'cms', 'pageuser'),
(7, 'cms', 'pageusergroup'),
(1, 'cms', 'placeholder'),
(10, 'cms', 'placeholderreference'),
(11, 'cms', 'staticplaceholder'),
(9, 'cms', 'title'),
(13, 'cms', 'urlconfrevision'),
(3, 'cms', 'usersettings'),
(49, 'cmsplugin_css_background', 'cssbackground'),
(50, 'cmsplugin_css_background', 'filercssbackground'),
(18, 'contenttypes', 'contenttype'),
(30, 'djangocms_file', 'file'),
(31, 'djangocms_file', 'folder'),
(22, 'djangocms_flash', 'flash'),
(35, 'djangocms_googlemap', 'googlemap'),
(36, 'djangocms_googlemap', 'googlemapmarker'),
(37, 'djangocms_googlemap', 'googlemaproute'),
(38, 'djangocms_link', 'link'),
(39, 'djangocms_picture', 'picture'),
(40, 'djangocms_snippet', 'snippet'),
(41, 'djangocms_snippet', 'snippetptr'),
(42, 'djangocms_teaser', 'teaser'),
(43, 'djangocms_text_ckeditor', 'text'),
(47, 'djangocms_twitter', 'twitterrecententries'),
(48, 'djangocms_twitter', 'twittersearch'),
(44, 'djangocms_video', 'videoplayer'),
(45, 'djangocms_video', 'videosource'),
(46, 'djangocms_video', 'videotrack'),
(32, 'easy_thumbnails', 'source'),
(33, 'easy_thumbnails', 'thumbnail'),
(34, 'easy_thumbnails', 'thumbnaildimensions'),
(26, 'filer', 'clipboard'),
(27, 'filer', 'clipboarditem'),
(25, 'filer', 'file'),
(23, 'filer', 'folder'),
(24, 'filer', 'folderpermission'),
(28, 'filer', 'image'),
(29, 'filer', 'thumbnailoption'),
(14, 'menus', 'cachekey'),
(19, 'sessions', 'session'),
(21, 'sites', 'site');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2017-01-14 17:11:56.763391'),
(2, 'auth', '0001_initial', '2017-01-14 17:12:14.055719'),
(3, 'admin', '0001_initial', '2017-01-14 17:12:16.334556'),
(4, 'admin', '0002_logentry_remove_auto_add', '2017-01-14 17:12:16.509307'),
(5, 'contenttypes', '0002_remove_content_type_name', '2017-01-14 17:12:17.822463'),
(6, 'auth', '0002_alter_permission_name_max_length', '2017-01-14 17:12:18.601611'),
(7, 'auth', '0003_alter_user_email_max_length', '2017-01-14 17:12:19.533315'),
(8, 'auth', '0004_alter_user_username_opts', '2017-01-14 17:12:19.580716'),
(9, 'auth', '0005_alter_user_last_login_null', '2017-01-14 17:12:20.180687'),
(10, 'auth', '0006_require_contenttypes_0002', '2017-01-14 17:12:20.238850'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2017-01-14 17:12:20.286330'),
(12, 'sites', '0001_initial', '2017-01-14 17:12:20.639506'),
(13, 'cms', '0001_initial', '2017-01-14 17:12:41.033253'),
(14, 'cms', '0002_auto_20140816_1918', '2017-01-14 17:13:05.416407'),
(15, 'cms', '0003_auto_20140926_2347', '2017-01-14 17:13:06.285457'),
(16, 'cms', '0004_auto_20140924_1038', '2017-01-14 17:13:14.704157'),
(17, 'cms', '0005_auto_20140924_1039', '2017-01-14 17:13:14.808632'),
(18, 'cms', '0006_auto_20140924_1110', '2017-01-14 17:13:23.241002'),
(19, 'cms', '0007_auto_20141028_1559', '2017-01-14 17:13:23.619946'),
(20, 'cms', '0008_auto_20150208_2149', '2017-01-14 17:13:23.795783'),
(21, 'cms', '0008_auto_20150121_0059', '2017-01-14 17:13:24.273859'),
(22, 'cms', '0009_merge', '2017-01-14 17:13:24.339762'),
(23, 'cms', '0010_migrate_use_structure', '2017-01-14 17:13:24.573406'),
(24, 'cms', '0011_auto_20150419_1006', '2017-01-14 17:13:27.331635'),
(25, 'cms', '0012_auto_20150607_2207', '2017-01-14 17:13:30.112197'),
(26, 'cms', '0013_urlconfrevision', '2017-01-14 17:13:30.407968'),
(27, 'cms', '0014_auto_20160404_1908', '2017-01-14 17:13:30.504776'),
(28, 'cms', '0015_auto_20160421_0000', '2017-01-14 17:13:31.367481'),
(29, 'cms', '0016_auto_20160608_1535', '2017-01-14 17:13:33.447509'),
(30, 'menus', '0001_initial', '2017-01-14 17:13:33.817104'),
(31, 'sessions', '0001_initial', '2017-01-14 17:13:34.405461'),
(32, 'sites', '0002_alter_domain_unique', '2017-01-14 17:13:34.857234'),
(33, 'djangocms_flash', '0001_initial', '2017-01-15 17:17:38.480582'),
(34, 'filer', '0001_initial', '2017-01-15 17:32:32.736464'),
(35, 'filer', '0002_auto_20150606_2003', '2017-01-15 17:32:33.933274'),
(36, 'filer', '0003_thumbnailoption', '2017-01-15 17:32:34.262114'),
(37, 'filer', '0004_auto_20160328_1434', '2017-01-15 17:32:35.061073'),
(38, 'filer', '0005_auto_20160623_1425', '2017-01-15 17:32:38.260782'),
(39, 'filer', '0006_auto_20160623_1627', '2017-01-15 17:32:39.259369'),
(40, 'djangocms_file', '0001_initial', '2017-01-15 17:32:40.692160'),
(41, 'djangocms_file', '0002_auto_20151202_1551', '2017-01-15 17:32:41.857627'),
(42, 'djangocms_file', '0003_remove_related_name_for_cmsplugin_ptr', '2017-01-15 17:32:42.863585'),
(43, 'djangocms_file', '0004_set_related_name_for_cmsplugin_ptr', '2017-01-15 17:32:43.937219'),
(44, 'djangocms_file', '0005_auto_20160119_1534', '2017-01-15 17:32:44.077610'),
(45, 'djangocms_file', '0006_migrate_to_filer', '2017-01-15 17:32:47.156003'),
(46, 'djangocms_file', '0007_adapted_fields', '2017-01-15 17:32:52.082682'),
(47, 'djangocms_file', '0008_add_folder', '2017-01-15 17:32:54.654037'),
(48, 'djangocms_file', '0009_fixed_null_fields', '2017-01-15 17:32:54.715408'),
(49, 'djangocms_file', '0010_removed_null_fields', '2017-01-15 17:32:55.632445'),
(50, 'easy_thumbnails', '0001_initial', '2017-01-15 17:33:18.506500'),
(51, 'easy_thumbnails', '0002_thumbnaildimensions', '2017-01-15 17:33:19.666680'),
(52, 'filer', '0007_auto_20161016_1055', '2017-01-15 17:33:19.740687'),
(53, 'djangocms_googlemap', '0001_initial', '2017-01-15 17:34:46.502642'),
(54, 'djangocms_googlemap', '0002_auto_20160622_1031', '2017-01-15 17:34:48.585697'),
(55, 'djangocms_googlemap', '0003_auto_20160825_1829', '2017-01-15 17:34:48.781622'),
(56, 'djangocms_googlemap', '0004_adapted_fields', '2017-01-15 17:35:00.943641'),
(57, 'djangocms_googlemap', '0005_create_nested_plugins', '2017-01-15 17:35:01.031244'),
(58, 'djangocms_googlemap', '0006_remove_fields', '2017-01-15 17:35:05.431885'),
(59, 'djangocms_googlemap', '0007_reset_null_values', '2017-01-15 17:35:05.476164'),
(60, 'djangocms_googlemap', '0008_removed_null_fields', '2017-01-15 17:35:06.244487'),
(61, 'djangocms_flash', '0002_auto_20170115_1735', '2017-01-15 17:36:00.776446'),
(62, 'djangocms_link', '0001_initial', '2017-01-15 17:36:56.202544'),
(63, 'djangocms_link', '0002_auto_20140929_1705', '2017-01-15 17:36:56.392462'),
(64, 'djangocms_link', '0003_auto_20150212_1310', '2017-01-15 17:36:56.570828'),
(65, 'djangocms_link', '0004_auto_20150708_1133', '2017-01-15 17:36:57.048485'),
(66, 'djangocms_link', '0005_auto_20151003_1710', '2017-01-15 17:36:58.036724'),
(67, 'djangocms_link', '0006_remove_related_name_for_cmsplugin_ptr', '2017-01-15 17:36:59.127380'),
(68, 'djangocms_link', '0007_set_related_name_for_cmsplugin_ptr', '2017-01-15 17:37:00.117692'),
(69, 'djangocms_link', '0008_link_attributes', '2017-01-15 17:37:00.871000'),
(70, 'djangocms_link', '0009_auto_20160705_1344', '2017-01-15 17:37:01.104723'),
(71, 'djangocms_link', '0010_adapted_fields', '2017-01-15 17:37:05.370448'),
(72, 'djangocms_link', '0011_fixed_null_values', '2017-01-15 17:37:05.432085'),
(73, 'djangocms_link', '0012_removed_null', '2017-01-15 17:37:09.167577'),
(74, 'djangocms_link', '0013_fix_hostname', '2017-01-15 17:37:09.494107'),
(75, 'djangocms_picture', '0001_initial', '2017-01-15 17:38:32.867994'),
(76, 'djangocms_picture', '0002_auto_20151018_1927', '2017-01-15 17:38:34.535496'),
(77, 'djangocms_picture', '0003_migrate_to_filer', '2017-01-15 17:38:39.445996'),
(78, 'djangocms_picture', '0004_adapt_fields', '2017-01-15 17:38:55.428439'),
(79, 'djangocms_picture', '0005_reset_null_values', '2017-01-15 17:38:55.522714'),
(80, 'djangocms_picture', '0006_remove_null_values', '2017-01-15 17:38:58.237874'),
(81, 'djangocms_picture', '0007_fix_alignment', '2017-01-15 17:38:58.457436'),
(82, 'djangocms_snippet', '0001_initial', '2017-01-15 17:40:16.690779'),
(83, 'djangocms_snippet', '0002_snippet_slug', '2017-01-15 17:40:17.795660'),
(84, 'djangocms_snippet', '0003_auto_data_fill_slug', '2017-01-15 17:40:17.859451'),
(85, 'djangocms_snippet', '0004_auto_alter_slug_unique', '2017-01-15 17:40:18.365792'),
(86, 'djangocms_snippet', '0005_set_related_name_for_cmsplugin_ptr', '2017-01-15 17:40:19.521750'),
(87, 'djangocms_snippet', '0006_auto_20160831_0729', '2017-01-15 17:40:21.907304'),
(88, 'djangocms_snippet', '0007_auto_alter_template_helptext', '2017-01-15 17:40:22.136482'),
(89, 'djangocms_teaser', '0001_initial', '2017-01-15 17:41:42.130395'),
(90, 'djangocms_text_ckeditor', '0001_initial', '2017-01-15 17:42:53.962170'),
(91, 'djangocms_text_ckeditor', '0002_remove_related_name_for_cmsplugin_ptr', '2017-01-15 17:42:55.030317'),
(92, 'djangocms_text_ckeditor', '0003_set_related_name_for_cmsplugin_ptr', '2017-01-15 17:42:55.970585'),
(93, 'djangocms_text_ckeditor', '0004_auto_20160706_1339', '2017-01-15 17:42:56.048428'),
(94, 'djangocms_video', '0001_initial', '2017-01-15 17:44:29.185841'),
(95, 'djangocms_video', '0002_set_related_name_for_cmsplugin_ptr', '2017-01-15 17:44:30.279704'),
(96, 'djangocms_video', '0003_field_adaptions', '2017-01-15 17:44:36.344701'),
(97, 'djangocms_video', '0004_move_to_attributes', '2017-01-15 17:44:45.994245'),
(98, 'djangocms_video', '0005_migrate_to_filer', '2017-01-15 17:44:46.058114'),
(99, 'djangocms_video', '0006_field_adaptions', '2017-01-15 17:44:52.608042'),
(100, 'djangocms_video', '0007_create_nested_plugin', '2017-01-15 17:44:53.333158'),
(101, 'djangocms_video', '0008_reset_null_values', '2017-01-15 17:44:53.402212'),
(102, 'djangocms_video', '0009_removed_null_values', '2017-01-15 17:44:54.175478'),
(103, 'djangocms_twitter', '0001_initial', '2017-01-15 17:45:30.165692'),
(104, 'djangocms_teaser', '0002_auto_20170115_1747', '2017-01-15 17:47:21.944139'),
(105, 'djangocms_twitter', '0002_auto_20170115_1747', '2017-01-15 17:47:24.180201'),
(106, 'cmsplugin_css_background', '0001_initial', '2017-01-17 17:53:50.186241'),
(107, 'cmsplugin_css_background', '0002_filercssbackground', '2017-01-17 17:53:53.946773');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('10ylxwqbz0q6tgnlh6a406nasnhpzw7u', 'N2MzOGRhMWFkNWIwNzVkMTNhNTE2MzFjYTUwOTY5ZjllNDBkODIzMTp7ImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiX2F1dGhfdXNlcl9oYXNoIjoiOTFmMzhhZjVkOTkzMGYxMWIwYzIwZWE2N2FkNGY0NGI0YjlmZWI2ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiZmlsZXJfbGFzdF9mb2xkZXJfaWQiOm51bGwsImNtc19hZG1pbl9zaXRlIjoxLCJjbXNfZWRpdCI6dHJ1ZSwiX2F1dGhfdXNlcl9pZCI6IjEifQ==', '2017-02-01 16:55:52.669280'),
('1l8psg0cjz4b38dcn68pxoy9ogabzphk', 'ZTQ2YzNhOTI0OTE4M2MzYzU2NzU2MWVlNWJkNzE1MzdiNzIwODIyMDp7Il9hdXRoX3VzZXJfaGFzaCI6IjJiZDRiNWFlNjQ1ODE1YTRjZTg2YWZlNTc2ZTY4NThmOTBiMTA2MDEiLCJ3aXphcmRfd2l6YXJkX2NyZWF0ZV92aWV3Ijp7ImV4dHJhX2RhdGEiOnt9LCJzdGVwX2RhdGEiOnsiMCI6eyJjc3JmbWlkZGxld2FyZXRva2VuIjpbIkNMb0xkZUZQOUFkeU1wQXRLaFc1cGdHNWF1azVObmprIl0sIjAtZW50cnkiOlsiZjdiNzdkMjhmODNiYWM1YjMxMzViODg1YTFiMzhiNGFmOTM0MmQyYSJdLCIwLWxhbmd1YWdlIjpbInJ1Il0sIjAtcGFnZSI6WyIzIl0sIndpemFyZF9jcmVhdGVfdmlldy1jdXJyZW50X3N0ZXAiOlsiMCJdfX0sInN0ZXBfZmlsZXMiOnsiMCI6e319LCJzdGVwIjoiMSJ9LCJfYXV0aF91c2VyX2lkIjoiMSIsImNtc19hZG1pbl9zaXRlIjoxLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiY21zX2VkaXQiOnRydWV9', '2017-01-28 19:19:37.528039'),
('x8kc0xa8076xug7ugtme80jqqo1jsxmy', 'ZjExNGQ0ZTQ4ZTQ2NzQ0MjdlZjA3ZjQyZDVlNGIzMTU2M2UyZmExNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjkxZjM4YWY1ZDk5MzBmMTFiMGMyMGVhNjdhZDRmNDRiNGI5ZmViNmYiLCJjbXNfdG9vbGJhcl9kaXNhYmxlZCI6ZmFsc2UsImNtc19hZG1pbl9zaXRlIjoxLCJmaWxlcl9sYXN0X2ZvbGRlcl9pZCI6bnVsbCwiX2F1dGhfdXNlcl9pZCI6IjEiLCJjbXNfZWRpdCI6ZmFsc2UsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2017-02-02 02:54:58.806481'),
('xxu1qyaxk2hsququuh8tp30vsagibnl3', 'ZjFjOWViZDQ1MWFhZDVkN2U5ZjJkOWFiNTE2MjcwMTE4ZjZiYzliYjp7ImNtc190b29sYmFyX2Rpc2FibGVkIjpmYWxzZSwiZmlsZXJfbGFzdF9mb2xkZXJfaWQiOm51bGwsImNtc19hZG1pbl9zaXRlIjoxLCJ3aXphcmRfd2l6YXJkX2NyZWF0ZV92aWV3Ijp7InN0ZXAiOiIwIiwic3RlcF9kYXRhIjp7fSwic3RlcF9maWxlcyI6e30sImV4dHJhX2RhdGEiOnt9fSwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiIyYWUxZjIwODljOGNjZjRjZWQxZDEyNDJlOTZiMWE5NzAwNGExOTE4IiwiY21zX2VkaXQiOmZhbHNlLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9', '2017-02-01 20:29:44.741622');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'beauty-salon-kate.ru', 'beauty-salon-kate.ru');

-- --------------------------------------------------------

--
-- Table structure for table `easy_thumbnails_source`
--

CREATE TABLE `easy_thumbnails_source` (
  `id` int(11) NOT NULL,
  `storage_hash` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `easy_thumbnails_source`
--

INSERT INTO `easy_thumbnails_source` (`id`, `storage_hash`, `name`, `modified`) VALUES
(1, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png', '2017-01-16 17:23:58.387695'),
(2, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png', '2017-01-16 17:13:13.582773'),
(3, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png', '2017-01-16 17:28:49.250361'),
(4, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg', '2017-01-16 17:37:52.946588'),
(5, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg', '2017-01-17 18:36:58.612346'),
(6, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg', '2017-01-17 18:42:51.184754'),
(7, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg', '2017-01-18 18:27:17.778045'),
(8, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg', '2017-01-18 18:56:06.913750'),
(9, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg', '2017-01-18 18:58:44.080890'),
(10, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg', '2017-01-18 19:06:01.243273'),
(11, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png', '2017-01-18 19:25:45.305271'),
(12, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png', '2017-01-18 19:26:20.627889'),
(13, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg', '2017-01-18 19:27:37.126920'),
(14, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png', '2017-01-18 19:28:26.679141'),
(15, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg', '2017-01-18 19:30:18.329539'),
(16, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg', '2017-01-18 19:34:02.472907'),
(17, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg', '2017-01-18 20:14:39.844336'),
(18, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg', '2017-01-18 20:15:35.257613'),
(19, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg', '2017-01-18 20:17:06.789815'),
(20, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg', '2017-01-18 20:18:12.387750'),
(21, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg', '2017-01-18 20:18:53.384421'),
(22, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg', '2017-01-18 20:19:36.331491'),
(23, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png', '2017-01-19 02:54:19.457671');

-- --------------------------------------------------------

--
-- Table structure for table `easy_thumbnails_thumbnail`
--

CREATE TABLE `easy_thumbnails_thumbnail` (
  `id` int(11) NOT NULL,
  `storage_hash` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime(6) NOT NULL,
  `source_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `easy_thumbnails_thumbnail`
--

INSERT INTO `easy_thumbnails_thumbnail` (`id`, `storage_hash`, `name`, `modified`, `source_id`) VALUES
(1, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:01.696466', 1),
(2, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:01.734479', 1),
(3, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:01.811670', 1),
(4, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:01.882452', 1),
(5, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:01.970011', 1),
(6, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:13.654980', 2),
(7, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:13.726920', 2),
(8, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:13.773383', 2),
(9, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:13.814556', 2),
(10, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:13:13.899299', 2),
(11, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:15:01.917051', 3),
(12, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:15:01.955355', 3),
(13, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:15:01.998944', 3),
(14, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:15:02.112961', 3),
(15, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-16 17:15:02.241981', 3),
(16, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png__125x13_q85_subsampling-2.png', '2017-01-16 17:23:58.466842', 1),
(17, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png__125x13_q85_subsampling-2.png', '2017-01-16 17:28:49.312656', 3),
(18, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-16 17:37:16.215633', 4),
(19, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-16 17:37:16.273147', 4),
(20, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-16 17:37:16.324090', 4),
(21, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-16 17:37:16.450682', 4),
(22, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-16 17:37:16.501348', 4),
(23, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg__300x300_q85_subsampling-2.jpg', '2017-01-16 17:37:52.978292', 4),
(24, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:36:22.487246', 5),
(25, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:36:22.606713', 5),
(26, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:36:22.655986', 5),
(27, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:36:22.696681', 5),
(28, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:36:22.808765', 5),
(29, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg__300x300_q85_subsampling-2.jpg', '2017-01-17 18:36:58.645788', 5),
(30, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:42:14.702112', 6),
(31, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:42:14.819871', 6),
(32, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:42:14.864163', 6),
(33, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:42:14.948068', 6),
(34, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-17 18:42:15.101672', 6),
(35, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg__300x300_q85_subsampling-2.jpg', '2017-01-17 18:42:51.243001', 6),
(36, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:26:27.122134', 7),
(37, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:26:27.221650', 7),
(38, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:26:27.278657', 7),
(39, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:26:27.340165', 7),
(40, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:26:27.447241', 7),
(41, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg__600x450_q85_subsampling-2.jpg', '2017-01-18 18:27:17.888307', 7),
(42, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:55:33.636884', 8),
(43, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:55:33.714190', 8),
(44, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:55:33.762188', 8),
(45, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:55:33.813487', 8),
(46, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:55:34.013217', 8),
(47, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg__252x312_q85_subsampling-2.jpg', '2017-01-18 18:56:06.983140', 8),
(48, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:58:21.558802', 9),
(49, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:58:21.634952', 9),
(50, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:58:21.684560', 9),
(51, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:58:21.735685', 9),
(52, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 18:58:21.805907', 9),
(53, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg__319x306_q85_subsampling-2.jpg', '2017-01-18 18:58:44.161573', 9),
(54, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:05:46.220501', 10),
(55, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:05:46.320096', 10),
(56, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:05:46.379397', 10),
(57, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:05:46.457851', 10),
(58, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:05:46.545120', 10),
(59, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg__298x300_q85_subsampling-2.jpg', '2017-01-18 19:06:01.336134', 10),
(60, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:25:31.317273', 11),
(61, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:25:31.353613', 11),
(62, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:25:31.421746', 11),
(63, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:25:31.485366', 11),
(64, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:25:31.592000', 11),
(65, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png__32x32_q85_subsampling-2.png', '2017-01-18 19:25:45.376930', 11),
(66, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:26:08.169576', 12),
(67, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:26:08.206349', 12),
(68, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:26:08.265622', 12),
(69, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:26:08.361449', 12),
(70, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:26:08.454036', 12),
(71, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png__32x32_q85_subsampling-2.png', '2017-01-18 19:26:20.661643', 12),
(72, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:26:58.759296', 13),
(73, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:26:58.830004', 13),
(74, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:26:58.873440', 13),
(75, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:26:58.923999', 13),
(76, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:26:58.984878', 13),
(77, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg__120x120_q85_subsampling-2.jpg', '2017-01-18 19:27:37.179356', 13),
(78, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:28:04.316261', 14),
(79, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:28:04.353990', 14),
(80, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:28:04.428851', 14),
(81, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:28:04.480086', 14),
(82, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-18 19:28:04.549224', 14),
(83, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png__32x32_q85_subsampling-2.png', '2017-01-18 19:28:26.769918', 14),
(84, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:29:17.088401', 15),
(85, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:29:17.159363', 15),
(86, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:29:17.203570', 15),
(87, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:29:17.277344', 15),
(88, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:29:17.326918', 15),
(89, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg__120x120_q85_subsampling-2.jpg', '2017-01-18 19:30:18.384656', 15),
(90, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:33:45.490435', 16),
(91, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:33:45.545059', 16),
(92, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:33:45.595121', 16),
(93, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:33:45.737578', 16),
(94, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 19:33:45.796329', 16),
(95, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg__120x120_q85_subsampling-2.jpg', '2017-01-18 19:34:02.509149', 16),
(96, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:13:29.447951', 17),
(97, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:13:29.541153', 17),
(98, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:13:29.662225', 17),
(99, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:13:29.711844', 17),
(100, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:13:29.780960', 17),
(101, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:14:39.880714', 17),
(102, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:15:14.528197', 18),
(103, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:15:14.582085', 18),
(104, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:15:14.644698', 18),
(105, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:15:14.703247', 18),
(106, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:15:14.893251', 18),
(107, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:15:35.357482', 18),
(108, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:16:48.250567', 19),
(109, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:16:48.301428', 19),
(110, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:16:48.352592', 19),
(111, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:16:48.452636', 19),
(112, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:16:48.525552', 19),
(113, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:17:06.863510', 19),
(114, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:17:49.556895', 20),
(115, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:17:49.613041', 20),
(116, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:17:49.721775', 20),
(117, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:17:49.913567', 20),
(118, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:17:50.043284', 20),
(119, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:18:12.435613', 20),
(120, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:18:33.617897', 21),
(121, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:18:33.663991', 21),
(122, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:18:33.715553', 21),
(123, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:18:33.774342', 21),
(124, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:18:33.910298', 21),
(125, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:18:53.417166', 21),
(126, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__16x16_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:19:20.861759', 22),
(127, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__32x32_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:19:20.915788', 22),
(128, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__48x48_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:19:20.974947', 22),
(129, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__64x64_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:19:21.026858', 22),
(130, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__180x180_q85_crop_subsampling-2_upscale.jpg', '2017-01-18 20:19:21.086964', 22),
(131, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg__400x400_q85_subsampling-2.jpg', '2017-01-18 20:19:36.424604', 22),
(132, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__16x16_q85_crop_subsampling-2_upscale.png', '2017-01-19 02:54:12.129048', 23),
(133, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__32x32_q85_crop_subsampling-2_upscale.png', '2017-01-19 02:54:12.167758', 23),
(134, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__48x48_q85_crop_subsampling-2_upscale.png', '2017-01-19 02:54:12.214865', 23),
(135, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__64x64_q85_crop_subsampling-2_upscale.png', '2017-01-19 02:54:12.263301', 23),
(136, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__180x180_q85_crop_subsampling-2_upscale.png', '2017-01-19 02:54:12.346626', 23),
(137, 'f9bde26a1556cd667f742bd34ec7c55e', 'filer_public_thumbnails/filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png__125x13_q85_subsampling-2.png', '2017-01-19 02:54:19.529263', 23);

-- --------------------------------------------------------

--
-- Table structure for table `easy_thumbnails_thumbnaildimensions`
--

CREATE TABLE `easy_thumbnails_thumbnaildimensions` (
  `id` int(11) NOT NULL,
  `thumbnail_id` int(11) NOT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filer_clipboard`
--

CREATE TABLE `filer_clipboard` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `filer_clipboard`
--

INSERT INTO `filer_clipboard` (`id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `filer_clipboarditem`
--

CREATE TABLE `filer_clipboarditem` (
  `id` int(11) NOT NULL,
  `clipboard_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filer_file`
--

CREATE TABLE `filer_file` (
  `id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_file_size` int(11) DEFAULT NULL,
  `sha1` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `has_all_mandatory_data` tinyint(1) NOT NULL,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `uploaded_at` datetime(6) NOT NULL,
  `modified_at` datetime(6) NOT NULL,
  `is_public` tinyint(1) NOT NULL,
  `folder_id` int(11),
  `owner_id` int(11),
  `polymorphic_ctype_id` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `filer_file`
--

INSERT INTO `filer_file` (`id`, `file`, `_file_size`, `sha1`, `has_all_mandatory_data`, `original_filename`, `name`, `description`, `uploaded_at`, `modified_at`, `is_public`, `folder_id`, `owner_id`, `polymorphic_ctype_id`) VALUES
(1, 'filer_public/ec/f1/ecf135c2-38ea-46aa-988e-9f4464ea29a2/border.png', 4174, 'a866139c705f62223d6e5faa6a66452dde1be5d0', 0, 'border.png', '', NULL, '2017-01-16 17:13:01.133770', '2017-01-16 17:13:01.133824', 1, NULL, 1, 28),
(2, 'filer_public/f5/1f/f51f6031-e364-4755-a2b7-12e676a6a8e6/border.png', 4174, 'a866139c705f62223d6e5faa6a66452dde1be5d0', 0, 'border.png', '', NULL, '2017-01-16 17:13:13.202965', '2017-01-16 17:13:13.203026', 1, NULL, 1, 28),
(3, 'filer_public/aa/4a/aa4a4725-5457-4d8a-8b82-2d84c5361f35/border.png', 4174, 'a866139c705f62223d6e5faa6a66452dde1be5d0', 0, 'border.png', '', NULL, '2017-01-16 17:15:01.824862', '2017-01-16 17:15:01.824925', 1, NULL, 1, 28),
(4, 'filer_public/9a/fc/9afc2dd3-f27b-43d1-93d2-80369f8b60b7/b1.jpg', 17345, '0fab6fd97a4a49280b9d1469a08764e1cfdf26a3', 0, 'b1.jpg', '', NULL, '2017-01-16 17:37:15.696886', '2017-01-16 17:37:15.696952', 1, NULL, 1, 28),
(5, 'filer_public/ad/31/ad31562b-6508-4cba-84ce-ff4e9a12c511/b2.jpg', 14100, 'd15a972c1d60d2d3ed66258fe4c18924687b97ac', 0, 'b2.jpg', '', NULL, '2017-01-17 18:36:22.025483', '2017-01-17 18:36:22.025544', 1, NULL, 1, 28),
(6, 'filer_public/c6/82/c68212a2-5cd4-4fbd-aee5-dd0e4c99b5c8/b3.jpg', 11397, '079450c21c9250dd2c20acdaf011dc5e07a49e5e', 0, 'b3.jpg', '', NULL, '2017-01-17 18:42:14.495296', '2017-01-17 18:42:14.495359', 1, NULL, 1, 28),
(7, 'filer_public/11/00/1100d3b8-19b3-4000-ac66-644e4536fde4/87.jpg', 156198, '38e0eca85d9f28a440afa2f2ca2db4b8178306d0', 0, '8(7).jpg', '', NULL, '2017-01-18 18:26:26.613769', '2017-01-18 18:26:26.613835', 1, NULL, 1, 28),
(8, 'filer_public/27/32/273224ff-b23f-49db-b3d2-a76348171a44/hair.jpg', 14525, 'e2c6772628f6755fd505428900397e1b3e6c6ee9', 0, 'hair.jpg', '', NULL, '2017-01-18 18:55:33.212006', '2017-01-18 18:55:33.212066', 1, NULL, 1, 28),
(9, 'filer_public/3e/87/3e878bc8-2076-4f5a-9587-9bbea2a3be2c/nxp9v_a4gz0.jpg', 24789, '3a5572b5585da5fb4b3750e81e8cd07810dfe684', 0, 'nXp9V_a4gZ0.jpg', '', NULL, '2017-01-18 18:58:21.442350', '2017-01-18 18:58:21.442411', 1, NULL, 1, 28),
(10, 'filer_public/19/a8/19a8d7fd-f575-48af-88b1-2100d3fea54b/nails.jpg', 17421, '119ae2d3df7ecffc4dac5d82175b4e670101ad48', 0, 'nails.jpg', '', NULL, '2017-01-18 19:05:46.054756', '2017-01-18 19:05:46.054816', 1, NULL, 1, 28),
(11, 'filer_public/95/e8/95e8006b-77dd-4fcb-acc0-427e2d62c654/left-quotes.png', 2200, 'ff1ae3da20b4442ec0b032f2ffdd7de6c54a4c11', 0, 'left-quotes.png', '', NULL, '2017-01-18 19:25:30.920099', '2017-01-18 19:25:30.920167', 1, NULL, 1, 28),
(12, 'filer_public/b7/d4/b7d4da75-c7e3-4a41-9ac9-2b9d99af1487/right-quotes.png', 2205, '9e447dba057b52040c1738ac0bfa2d425bebdc7d', 0, 'right-quotes.png', '', NULL, '2017-01-18 19:26:08.080788', '2017-01-18 19:26:08.080852', 1, NULL, 1, 28),
(13, 'filer_public/ca/19/ca19b2cb-11fa-42f0-9741-a0eb771291a8/1.jpg', 5086, '5a5a41c29f12bf115a5d005790e752da83582197', 0, '1.jpg', '', NULL, '2017-01-18 19:26:58.666978', '2017-01-18 19:26:58.667036', 1, NULL, 1, 28),
(14, 'filer_public/59/a0/59a08ef1-28a4-4ed2-a26b-eb06af7d31b2/left-quotes.png', 2200, 'ff1ae3da20b4442ec0b032f2ffdd7de6c54a4c11', 0, 'left-quotes.png', '', NULL, '2017-01-18 19:28:04.041867', '2017-01-18 19:28:04.041933', 1, NULL, 1, 28),
(15, 'filer_public/74/f1/74f15d9b-679d-4f5e-a3d9-e5e66f34b466/2.jpg', 4629, '532b1e7854ab8864bd61d69b0e827d8d19f7ae17', 0, '2.jpg', '', NULL, '2017-01-18 19:29:16.964278', '2017-01-18 19:29:16.964343', 1, NULL, 1, 28),
(16, 'filer_public/f9/73/f9738c64-0083-46c1-91fc-5e473ccc34a5/3.jpg', 4731, 'd2ec54fd6891f428b95bceac41fd725a052a8f29', 0, '3.jpg', '', NULL, '2017-01-18 19:33:45.362062', '2017-01-18 19:33:45.362123', 1, NULL, 1, 28),
(17, 'filer_public/ae/c8/aec8408c-162a-4ac4-b122-40e3962b4542/t1.jpg', 22146, 'ab2c4285c6b6bed6555452d43c46a5e43a36a345', 0, 't1.jpg', '', NULL, '2017-01-18 20:13:29.109856', '2017-01-18 20:13:29.109920', 1, NULL, 1, 28),
(18, 'filer_public/e7/79/e7797307-edeb-4bf9-b258-16d560a536d3/t2.jpg', 24731, '45ddc60a2d19ce99eb41eb66c95b80b12b9aeb19', 0, 't2.jpg', '', NULL, '2017-01-18 20:15:14.432603', '2017-01-18 20:15:14.432669', 1, NULL, 1, 28),
(19, 'filer_public/11/34/1134249d-3956-4120-b10d-fd45dbd5f335/t3.jpg', 27370, '027a6e323d43b6799c1250e5d3d26a3e9260ce19', 0, 't3.jpg', '', NULL, '2017-01-18 20:16:48.113148', '2017-01-18 20:16:48.113230', 1, NULL, 1, 28),
(20, 'filer_public/5c/f4/5cf4b877-c510-4852-bb71-69dab03877e4/t4.jpg', 23911, 'db10c5c84de7af73d8ef06bb39e95e93de409471', 0, 't4.jpg', '', NULL, '2017-01-18 20:17:49.435642', '2017-01-18 20:17:49.435706', 1, NULL, 1, 28),
(21, 'filer_public/36/c6/36c67208-444b-4bd2-a1dc-15c27e5f6c0d/t5.jpg', 20637, '21bbed9d83749d31c80dd87330fe3cd10d254b4c', 0, 't5.jpg', '', NULL, '2017-01-18 20:18:33.291567', '2017-01-18 20:18:33.291636', 1, NULL, 1, 28),
(22, 'filer_public/da/76/da767f65-2ed3-43fa-ac46-84c0bcfdd749/t6.jpg', 18250, 'd1bb9d6673887d7f63dde7eb6a53f416c092cd11', 0, 't6.jpg', '', NULL, '2017-01-18 20:19:20.654032', '2017-01-18 20:19:20.654099', 1, NULL, 1, 28),
(23, 'filer_public/e8/d4/e8d43828-0692-4c92-883d-c6f51f28c0f3/border2.png', 3822, '9d1a2be50e8b7635141d63413aaea9d0c6034d00', 0, 'border2.png', '', NULL, '2017-01-19 02:54:11.962464', '2017-01-19 02:54:11.962532', 1, NULL, 1, 28);

-- --------------------------------------------------------

--
-- Table structure for table `filer_folder`
--

CREATE TABLE `filer_folder` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded_at` datetime(6) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `modified_at` datetime(6) NOT NULL,
  `lft` int(10) UNSIGNED NOT NULL,
  `rght` int(10) UNSIGNED NOT NULL,
  `tree_id` int(10) UNSIGNED NOT NULL,
  `level` int(10) UNSIGNED NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filer_folderpermission`
--

CREATE TABLE `filer_folderpermission` (
  `id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `everybody` tinyint(1) NOT NULL,
  `can_edit` smallint(6) DEFAULT NULL,
  `can_read` smallint(6) DEFAULT NULL,
  `can_add_children` smallint(6) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `filer_image`
--

CREATE TABLE `filer_image` (
  `file_ptr_id` int(11) NOT NULL,
  `_height` int(11) DEFAULT NULL,
  `_width` int(11) DEFAULT NULL,
  `date_taken` datetime(6) DEFAULT NULL,
  `default_alt_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `must_always_publish_author_credit` tinyint(1) NOT NULL,
  `must_always_publish_copyright` tinyint(1) NOT NULL,
  `subject_location` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `filer_image`
--

INSERT INTO `filer_image` (`file_ptr_id`, `_height`, `_width`, `date_taken`, `default_alt_text`, `default_caption`, `author`, `must_always_publish_author_credit`, `must_always_publish_copyright`, `subject_location`) VALUES
(1, 13, 125, '2017-01-16 17:13:01.009117', NULL, NULL, NULL, 0, 0, ''),
(2, 13, 125, '2017-01-16 17:13:13.076776', NULL, NULL, NULL, 0, 0, ''),
(3, 13, 125, '2017-01-16 17:15:01.799282', NULL, NULL, NULL, 0, 0, ''),
(4, 300, 300, '2017-01-16 17:37:15.593264', NULL, NULL, NULL, 0, 0, ''),
(5, 300, 300, '2017-01-17 18:36:21.962305', NULL, NULL, NULL, 0, 0, ''),
(6, 300, 300, '2017-01-17 18:42:14.416467', NULL, NULL, NULL, 0, 0, ''),
(7, 450, 600, '2017-01-18 18:26:26.516621', NULL, NULL, NULL, 0, 0, ''),
(8, 312, 252, '2017-01-18 18:55:33.164635', NULL, NULL, NULL, 0, 0, ''),
(9, 306, 319, '2017-01-18 18:58:21.414134', NULL, NULL, NULL, 0, 0, ''),
(10, 300, 298, '2017-01-18 19:05:46.047247', NULL, NULL, NULL, 0, 0, ''),
(11, 32, 32, '2017-01-18 19:25:30.912666', NULL, NULL, NULL, 0, 0, ''),
(12, 32, 32, '2017-01-18 19:26:08.063413', NULL, NULL, NULL, 0, 0, ''),
(13, 120, 120, '2017-01-18 19:26:58.641090', NULL, NULL, NULL, 0, 0, ''),
(14, 32, 32, '2017-01-18 19:28:04.014270', NULL, NULL, NULL, 0, 0, ''),
(15, 120, 120, '2017-01-18 19:29:16.945329', NULL, NULL, NULL, 0, 0, ''),
(16, 120, 120, '2017-01-18 19:33:45.356429', NULL, NULL, NULL, 0, 0, ''),
(17, 400, 400, '2017-01-18 20:13:29.074415', NULL, NULL, NULL, 0, 0, ''),
(18, 400, 400, '2017-01-18 20:15:14.426901', NULL, NULL, NULL, 0, 0, ''),
(19, 400, 400, '2017-01-18 20:16:48.095840', NULL, NULL, NULL, 0, 0, ''),
(20, 400, 400, '2017-01-18 20:17:49.427424', NULL, NULL, NULL, 0, 0, ''),
(21, 400, 400, '2017-01-18 20:18:33.280978', NULL, NULL, NULL, 0, 0, ''),
(22, 400, 400, '2017-01-18 20:19:20.641425', NULL, NULL, NULL, 0, 0, ''),
(23, 13, 125, '2017-01-19 02:54:11.781689', NULL, NULL, NULL, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `filer_thumbnailoption`
--

CREATE TABLE `filer_thumbnailoption` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `crop` tinyint(1) NOT NULL,
  `upscale` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus_cachekey`
--

CREATE TABLE `menus_cachekey` (
  `id` int(11) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Position`
--

CREATE TABLE `Position` (
  `id_position` int(11) NOT NULL,
  `position_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Position`
--

INSERT INTO `Position` (`id_position`, `position_name`) VALUES
(1, 'Директор'),
(2, 'Администратор'),
(3, 'Парикмахер'),
(4, 'Мастер маникюра');

-- --------------------------------------------------------

--
-- Table structure for table `Price`
--

CREATE TABLE `Price` (
  `id_staff` int(11) UNSIGNED NOT NULL,
  `id_service` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Price`
--

INSERT INTO `Price` (`id_staff`, `id_service`, `price`) VALUES
(2, 1, 500),
(2, 2, 1000),
(2, 3, 2500),
(4, 4, 1000),
(4, 5, 1200);

-- --------------------------------------------------------

--
-- Table structure for table `Services`
--

CREATE TABLE `Services` (
  `id_service` int(11) NOT NULL,
  `service_name` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Services`
--

INSERT INTO `Services` (`id_service`, `service_name`) VALUES
(1, 'Стрижка мужская'),
(2, 'Стрижка женская'),
(3, 'Окрашивание волос'),
(4, 'Маникюр'),
(5, 'Педикюр');

-- --------------------------------------------------------

--
-- Table structure for table `Staff`
--

CREATE TABLE `Staff` (
  `id_staff` int(10) UNSIGNED NOT NULL,
  `Full_name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Staff`
--

INSERT INTO `Staff` (`id_staff`, `Full_name`, `Phone`, `Address`) VALUES
(1, 'Иванов Иван Иванович', '89123456789', 'Екатеринбург, ул. Мира 54 кв 33'),
(2, 'Петров Петр Петрович', '+79215498736', 'Екатеринбург, ул. Белинского 222 кв 79'),
(4, 'Сулемейкин Аркадий Иванович', '89147852369', 'Кундус, ул. Арамзанзан 67 кв. 46');

-- --------------------------------------------------------

--
-- Table structure for table `Staff_position`
--

CREATE TABLE `Staff_position` (
  `id_staff` int(11) UNSIGNED NOT NULL,
  `id_position` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `salary` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Staff_position`
--

INSERT INTO `Staff_position` (`id_staff`, `id_position`, `date_start`, `salary`) VALUES
(1, 1, '2016-09-01', 50000),
(1, 2, '2016-09-02', 20000),
(2, 3, '2016-09-05', 20000),
(4, 4, '2016-09-20', 15000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`);

--
-- Indexes for table `cmsplugin_css_background_cssbackground`
--
ALTER TABLE `cmsplugin_css_background_cssbackground`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `cmsplugin_css_background_filercssbackground`
--
ALTER TABLE `cmsplugin_css_background_filercssbackground`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `cmsplugin_css_background_filercssbackground_f33175e6` (`image_id`);

--
-- Indexes for table `cms_aliaspluginmodel`
--
ALTER TABLE `cms_aliaspluginmodel`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `cms_aliaspluginmodel_921abf5c` (`alias_placeholder_id`),
  ADD KEY `cms_aliaspluginmodel_plugin_id_326643c3_fk_cms_cmsplugin_id` (`plugin_id`);

--
-- Indexes for table `cms_cmsplugin`
--
ALTER TABLE `cms_cmsplugin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_cmsplugin_path_4917bb44_uniq` (`path`),
  ADD KEY `cms_cmsplugin_8512ae7d` (`language`),
  ADD KEY `cms_cmsplugin_b5e4cf8f` (`plugin_type`),
  ADD KEY `cms_cmsplugin_6be37982` (`parent_id`),
  ADD KEY `cms_cmsplugin_667a6151` (`placeholder_id`);

--
-- Indexes for table `cms_globalpagepermission`
--
ALTER TABLE `cms_globalpagepermission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_globalpagepermission_group_id_991b4733_fk_auth_group_id` (`group_id`),
  ADD KEY `cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `cms_globalpagepermission_sites`
--
ALTER TABLE `cms_globalpagepermission_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_globalpagepermission_s_globalpagepermission_id_db684f41_uniq` (`globalpagepermission_id`,`site_id`),
  ADD KEY `cms_globalpagepermission_site_site_id_00460b53_fk_django_site_id` (`site_id`);

--
-- Indexes for table `cms_page`
--
ALTER TABLE `cms_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_page_path_d3a06f3d_uniq` (`path`),
  ADD UNIQUE KEY `publisher_public_id` (`publisher_public_id`),
  ADD UNIQUE KEY `cms_page_reverse_id_d5d1256a_uniq` (`reverse_id`,`site_id`,`publisher_is_draft`),
  ADD UNIQUE KEY `cms_page_publisher_is_draft_8c776642_uniq` (`publisher_is_draft`,`site_id`,`application_namespace`),
  ADD KEY `cms_page_parent_id_f89b72e4_fk_cms_page_id` (`parent_id`),
  ADD KEY `cms_page_site_id_4323d3ff_fk_django_site_id` (`site_id`),
  ADD KEY `cms_page_93b83098` (`publication_date`),
  ADD KEY `cms_page_2247c5f0` (`publication_end_date`),
  ADD KEY `cms_page_db3eb53f` (`in_navigation`),
  ADD KEY `cms_page_1d85575d` (`soft_root`),
  ADD KEY `cms_page_3d9ef52f` (`reverse_id`),
  ADD KEY `cms_page_7b8acfa6` (`navigation_extenders`),
  ADD KEY `cms_page_cb540373` (`limit_visibility_in_menu`),
  ADD KEY `cms_page_4fa1c803` (`is_home`),
  ADD KEY `cms_page_e721871e` (`application_urls`),
  ADD KEY `cms_page_b7700099` (`publisher_is_draft`);

--
-- Indexes for table `cms_pagepermission`
--
ALTER TABLE `cms_pagepermission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_pagepermission_group_id_af5af193_fk_auth_group_id` (`group_id`),
  ADD KEY `cms_pagepermission_page_id_0ae9a163_fk_cms_page_id` (`page_id`),
  ADD KEY `cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `cms_pageuser`
--
ALTER TABLE `cms_pageuser`
  ADD PRIMARY KEY (`user_ptr_id`),
  ADD KEY `cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `cms_pageusergroup`
--
ALTER TABLE `cms_pageusergroup`
  ADD PRIMARY KEY (`group_ptr_id`),
  ADD KEY `cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `cms_page_placeholders`
--
ALTER TABLE `cms_page_placeholders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_page_placeholders_page_id_ab7fbfb8_uniq` (`page_id`,`placeholder_id`),
  ADD KEY `cms_page_placehold_placeholder_id_6b120886_fk_cms_placeholder_id` (`placeholder_id`);

--
-- Indexes for table `cms_placeholder`
--
ALTER TABLE `cms_placeholder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_placeholder_5e97994e` (`slot`);

--
-- Indexes for table `cms_placeholderreference`
--
ALTER TABLE `cms_placeholderreference`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `cms_placeholderreference_328d0afc` (`placeholder_ref_id`);

--
-- Indexes for table `cms_staticplaceholder`
--
ALTER TABLE `cms_staticplaceholder`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_staticplaceholder_code_21ba079c_uniq` (`code`,`site_id`),
  ADD KEY `cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id` (`site_id`),
  ADD KEY `cms_staticplaceholder_5cb48773` (`draft_id`),
  ADD KEY `cms_staticplaceholder_1ee2744d` (`public_id`);

--
-- Indexes for table `cms_title`
--
ALTER TABLE `cms_title`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_title_language_61aaf084_uniq` (`language`,`page_id`),
  ADD UNIQUE KEY `publisher_public_id` (`publisher_public_id`),
  ADD KEY `cms_title_page_id_5fade2a3_fk_cms_page_id` (`page_id`),
  ADD KEY `cms_title_8512ae7d` (`language`),
  ADD KEY `cms_title_2dbcba41` (`slug`),
  ADD KEY `cms_title_d6fe1d0b` (`path`),
  ADD KEY `cms_title_1268de9a` (`has_url_overwrite`),
  ADD KEY `cms_title_b7700099` (`publisher_is_draft`),
  ADD KEY `cms_title_f7202fc0` (`publisher_state`);

--
-- Indexes for table `cms_urlconfrevision`
--
ALTER TABLE `cms_urlconfrevision`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_usersettings`
--
ALTER TABLE `cms_usersettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id` (`clipboard_id`);

--
-- Indexes for table `djangocms_file_file`
--
ALTER TABLE `djangocms_file_file`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_file_file_1b1bd9f0` (`file_src_id`);

--
-- Indexes for table `djangocms_file_folder`
--
ALTER TABLE `djangocms_file_folder`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_file_folder_a758a370` (`folder_src_id`);

--
-- Indexes for table `djangocms_flash_flash`
--
ALTER TABLE `djangocms_flash_flash`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_googlemap_googlemap`
--
ALTER TABLE `djangocms_googlemap_googlemap`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_googlemap_googlemapmarker`
--
ALTER TABLE `djangocms_googlemap_googlemapmarker`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_googlemap_googlemaproute`
--
ALTER TABLE `djangocms_googlemap_googlemaproute`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_link_link`
--
ALTER TABLE `djangocms_link_link`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_link_link_internal_link_id_1d69dd8e_fk_cms_page_id` (`internal_link_id`);

--
-- Indexes for table `djangocms_picture_picture`
--
ALTER TABLE `djangocms_picture_picture`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_picture_picture_c6fe9aa6` (`picture_id`),
  ADD KEY `djangocms_picture_picture_link_page_id_39dc2dcc_fk_cms_page_id` (`link_page_id`),
  ADD KEY `djangocms_picture_picture_94eaa149` (`thumbnail_options_id`);

--
-- Indexes for table `djangocms_snippet_snippet`
--
ALTER TABLE `djangocms_snippet_snippet`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `djangocms_snippet_snippet_slug_1220b1f8_uniq` (`slug`),
  ADD KEY `djangocms_snippet_snippet_2dbcba41` (`slug`);

--
-- Indexes for table `djangocms_snippet_snippetptr`
--
ALTER TABLE `djangocms_snippet_snippetptr`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_sn_snippet_id_4f251ac3_fk_djangocms_snippet_snippet_id` (`snippet_id`);

--
-- Indexes for table `djangocms_teaser_teaser`
--
ALTER TABLE `djangocms_teaser_teaser`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_teaser_teaser_page_link_id_716a6f2b_fk_cms_page_id` (`page_link_id`);

--
-- Indexes for table `djangocms_text_ckeditor_text`
--
ALTER TABLE `djangocms_text_ckeditor_text`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_twitter_twitterrecententries`
--
ALTER TABLE `djangocms_twitter_twitterrecententries`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_twitter_twittersearch`
--
ALTER TABLE `djangocms_twitter_twittersearch`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`);

--
-- Indexes for table `djangocms_video_videoplayer`
--
ALTER TABLE `djangocms_video_videoplayer`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_video_videoplayer_9b86e5fe` (`poster_id`);

--
-- Indexes for table `djangocms_video_videosource`
--
ALTER TABLE `djangocms_video_videosource`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_video_videosource_01490fd0` (`source_file_id`);

--
-- Indexes for table `djangocms_video_videotrack`
--
ALTER TABLE `djangocms_video_videotrack`
  ADD PRIMARY KEY (`cmsplugin_ptr_id`),
  ADD KEY `djangocms_video_videotrack_3166800b` (`src_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- Indexes for table `django_site`
--
ALTER TABLE `django_site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`);

--
-- Indexes for table `easy_thumbnails_source`
--
ALTER TABLE `easy_thumbnails_source`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `easy_thumbnails_source_storage_hash_36eac304_uniq` (`storage_hash`,`name`),
  ADD KEY `easy_thumbnails_source_b454e115` (`storage_hash`),
  ADD KEY `easy_thumbnails_source_b068931c` (`name`);

--
-- Indexes for table `easy_thumbnails_thumbnail`
--
ALTER TABLE `easy_thumbnails_thumbnail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `easy_thumbnails_thumbnail_storage_hash_15545a36_uniq` (`storage_hash`,`name`,`source_id`),
  ADD KEY `easy_thumbnails__source_id_4b310d48_fk_easy_thumbnails_source_id` (`source_id`),
  ADD KEY `easy_thumbnails_thumbnail_b454e115` (`storage_hash`),
  ADD KEY `easy_thumbnails_thumbnail_b068931c` (`name`);

--
-- Indexes for table `easy_thumbnails_thumbnaildimensions`
--
ALTER TABLE `easy_thumbnails_thumbnaildimensions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `thumbnail_id` (`thumbnail_id`);

--
-- Indexes for table `filer_clipboard`
--
ALTER TABLE `filer_clipboard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filer_clipboard_e8701ad4` (`user_id`);

--
-- Indexes for table `filer_clipboarditem`
--
ALTER TABLE `filer_clipboarditem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filer_clipboarditem_clipboard_id_3625ceaf_fk_filer_clipboard_id` (`clipboard_id`),
  ADD KEY `filer_clipboarditem_814552b9` (`file_id`);

--
-- Indexes for table `filer_file`
--
ALTER TABLE `filer_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filer_file_a8a44dbb` (`folder_id`),
  ADD KEY `filer_file_5e7b1936` (`owner_id`),
  ADD KEY `filer_file_d3e32c49` (`polymorphic_ctype_id`);

--
-- Indexes for table `filer_folder`
--
ALTER TABLE `filer_folder`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `filer_folder_parent_id_5d5c4935_uniq` (`parent_id`,`name`),
  ADD KEY `filer_folder_caf7cc51` (`lft`),
  ADD KEY `filer_folder_3cfbd988` (`rght`),
  ADD KEY `filer_folder_656442a0` (`tree_id`),
  ADD KEY `filer_folder_c9e9a848` (`level`),
  ADD KEY `filer_folder_owner_id_5d73fc39_fk_auth_user_id` (`owner_id`);

--
-- Indexes for table `filer_folderpermission`
--
ALTER TABLE `filer_folderpermission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `filer_folderpermission_folder_id_26643568_fk_filer_folder_id` (`folder_id`),
  ADD KEY `filer_folderpermission_group_id_81323fc_fk_auth_group_id` (`group_id`),
  ADD KEY `filer_folderpermission_user_id_521cc5a1_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `filer_image`
--
ALTER TABLE `filer_image`
  ADD PRIMARY KEY (`file_ptr_id`);

--
-- Indexes for table `filer_thumbnailoption`
--
ALTER TABLE `filer_thumbnailoption`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus_cachekey`
--
ALTER TABLE `menus_cachekey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Position`
--
ALTER TABLE `Position`
  ADD PRIMARY KEY (`id_position`);

--
-- Indexes for table `Price`
--
ALTER TABLE `Price`
  ADD PRIMARY KEY (`id_staff`,`id_service`),
  ADD KEY `id_service` (`id_service`);

--
-- Indexes for table `Services`
--
ALTER TABLE `Services`
  ADD PRIMARY KEY (`id_service`);

--
-- Indexes for table `Staff`
--
ALTER TABLE `Staff`
  ADD PRIMARY KEY (`id_staff`);

--
-- Indexes for table `Staff_position`
--
ALTER TABLE `Staff_position`
  ADD PRIMARY KEY (`id_staff`,`id_position`),
  ADD KEY `id_position` (`id_position`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_cmsplugin`
--
ALTER TABLE `cms_cmsplugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `cms_globalpagepermission`
--
ALTER TABLE `cms_globalpagepermission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_globalpagepermission_sites`
--
ALTER TABLE `cms_globalpagepermission_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page`
--
ALTER TABLE `cms_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cms_pagepermission`
--
ALTER TABLE `cms_pagepermission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page_placeholders`
--
ALTER TABLE `cms_page_placeholders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `cms_placeholder`
--
ALTER TABLE `cms_placeholder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `cms_staticplaceholder`
--
ALTER TABLE `cms_staticplaceholder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_title`
--
ALTER TABLE `cms_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cms_urlconfrevision`
--
ALTER TABLE `cms_urlconfrevision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_usersettings`
--
ALTER TABLE `cms_usersettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `djangocms_snippet_snippet`
--
ALTER TABLE `djangocms_snippet_snippet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `django_site`
--
ALTER TABLE `django_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `easy_thumbnails_source`
--
ALTER TABLE `easy_thumbnails_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `easy_thumbnails_thumbnail`
--
ALTER TABLE `easy_thumbnails_thumbnail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `easy_thumbnails_thumbnaildimensions`
--
ALTER TABLE `easy_thumbnails_thumbnaildimensions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filer_clipboard`
--
ALTER TABLE `filer_clipboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `filer_clipboarditem`
--
ALTER TABLE `filer_clipboarditem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filer_file`
--
ALTER TABLE `filer_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `filer_folder`
--
ALTER TABLE `filer_folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filer_folderpermission`
--
ALTER TABLE `filer_folderpermission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `filer_thumbnailoption`
--
ALTER TABLE `filer_thumbnailoption`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menus_cachekey`
--
ALTER TABLE `menus_cachekey`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Position`
--
ALTER TABLE `Position`
  MODIFY `id_position` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Services`
--
ALTER TABLE `Services`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Staff`
--
ALTER TABLE `Staff`
  MODIFY `id_staff` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `cmsplugin_css_background_cssbackground`
--
ALTER TABLE `cmsplugin_css_background_cssbackground`
  ADD CONSTRAINT `cmsplugin_css_back_cmsplugin_ptr_id_35d9d440_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `cmsplugin_css_background_filercssbackground`
--
ALTER TABLE `cmsplugin_css_background_filercssbackground`
  ADD CONSTRAINT `cmsplugin_css_back_cmsplugin_ptr_id_4ab71bfe_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cmsplugin_css_backg_image_id_798fafa5_fk_filer_image_file_ptr_id` FOREIGN KEY (`image_id`) REFERENCES `filer_image` (`file_ptr_id`);

--
-- Constraints for table `cms_aliaspluginmodel`
--
ALTER TABLE `cms_aliaspluginmodel`
  ADD CONSTRAINT `cms_aliasplu_alias_placeholder_id_6d6e0c8a_fk_cms_placeholder_id` FOREIGN KEY (`alias_placeholder_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_aliaspluginmod_cmsplugin_ptr_id_f71dfd31_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_aliaspluginmodel_plugin_id_326643c3_fk_cms_cmsplugin_id` FOREIGN KEY (`plugin_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `cms_cmsplugin`
--
ALTER TABLE `cms_cmsplugin`
  ADD CONSTRAINT `cms_cmsplugin_parent_id_556cc045_fk_cms_cmsplugin_id` FOREIGN KEY (`parent_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_id`) REFERENCES `cms_placeholder` (`id`);

--
-- Constraints for table `cms_globalpagepermission`
--
ALTER TABLE `cms_globalpagepermission`
  ADD CONSTRAINT `cms_globalpagepermission_group_id_991b4733_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `cms_globalpagepermission_sites`
--
ALTER TABLE `cms_globalpagepermission_sites`
  ADD CONSTRAINT `cms_globalpagepermission_site_site_id_00460b53_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`),
  ADD CONSTRAINT `globalpagepermission_id_46bd2681_fk_cms_globalpagepermission_id` FOREIGN KEY (`globalpagepermission_id`) REFERENCES `cms_globalpagepermission` (`id`);

--
-- Constraints for table `cms_page`
--
ALTER TABLE `cms_page`
  ADD CONSTRAINT `cms_page_parent_id_f89b72e4_fk_cms_page_id` FOREIGN KEY (`parent_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_page_publisher_public_id_d619fca0_fk_cms_page_id` FOREIGN KEY (`publisher_public_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_page_site_id_4323d3ff_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`);

--
-- Constraints for table `cms_pagepermission`
--
ALTER TABLE `cms_pagepermission`
  ADD CONSTRAINT `cms_pagepermission_group_id_af5af193_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `cms_pagepermission_page_id_0ae9a163_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `cms_pageuser`
--
ALTER TABLE `cms_pageuser`
  ADD CONSTRAINT `cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id` FOREIGN KEY (`user_ptr_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `cms_pageusergroup`
--
ALTER TABLE `cms_pageusergroup`
  ADD CONSTRAINT `cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id` FOREIGN KEY (`group_ptr_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `cms_page_placeholders`
--
ALTER TABLE `cms_page_placeholders`
  ADD CONSTRAINT `cms_page_placehold_placeholder_id_6b120886_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`);

--
-- Constraints for table `cms_placeholderreference`
--
ALTER TABLE `cms_placeholderreference`
  ADD CONSTRAINT `cms_placeholde_placeholder_ref_id_244759b1_fk_cms_placeholder_id` FOREIGN KEY (`placeholder_ref_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_placeholderref_cmsplugin_ptr_id_6977ec85_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `cms_staticplaceholder`
--
ALTER TABLE `cms_staticplaceholder`
  ADD CONSTRAINT `cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id` FOREIGN KEY (`draft_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id` FOREIGN KEY (`public_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`);

--
-- Constraints for table `cms_title`
--
ALTER TABLE `cms_title`
  ADD CONSTRAINT `cms_title_page_id_5fade2a3_fk_cms_page_id` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`id`),
  ADD CONSTRAINT `cms_title_publisher_public_id_003a2702_fk_cms_title_id` FOREIGN KEY (`publisher_public_id`) REFERENCES `cms_title` (`id`);

--
-- Constraints for table `cms_usersettings`
--
ALTER TABLE `cms_usersettings`
  ADD CONSTRAINT `cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id` FOREIGN KEY (`clipboard_id`) REFERENCES `cms_placeholder` (`id`),
  ADD CONSTRAINT `cms_usersettings_user_id_09633b2d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `djangocms_file_file`
--
ALTER TABLE `djangocms_file_file`
  ADD CONSTRAINT `djangocms_file_fil_cmsplugin_ptr_id_5c5b553d_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_file_file_file_src_id_5f36e8f6_fk_filer_file_id` FOREIGN KEY (`file_src_id`) REFERENCES `filer_file` (`id`);

--
-- Constraints for table `djangocms_file_folder`
--
ALTER TABLE `djangocms_file_folder`
  ADD CONSTRAINT `djangocms_file_fol_cmsplugin_ptr_id_7b70d8b8_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_file_folder_folder_src_id_7b712f80_fk_filer_folder_id` FOREIGN KEY (`folder_src_id`) REFERENCES `filer_folder` (`id`);

--
-- Constraints for table `djangocms_flash_flash`
--
ALTER TABLE `djangocms_flash_flash`
  ADD CONSTRAINT `djangocms_flash_fl_cmsplugin_ptr_id_354b7b10_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_googlemap_googlemap`
--
ALTER TABLE `djangocms_googlemap_googlemap`
  ADD CONSTRAINT `djangocms_googlema_cmsplugin_ptr_id_27f9f1f9_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_googlemap_googlemapmarker`
--
ALTER TABLE `djangocms_googlemap_googlemapmarker`
  ADD CONSTRAINT `djangocms_googlema_cmsplugin_ptr_id_24b21853_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_googlemap_googlemaproute`
--
ALTER TABLE `djangocms_googlemap_googlemaproute`
  ADD CONSTRAINT `djangocms_googlema_cmsplugin_ptr_id_31fa9552_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_link_link`
--
ALTER TABLE `djangocms_link_link`
  ADD CONSTRAINT `djangocms_link_lin_cmsplugin_ptr_id_4c2c082c_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_link_link_internal_link_id_1d69dd8e_fk_cms_page_id` FOREIGN KEY (`internal_link_id`) REFERENCES `cms_page` (`id`);

--
-- Constraints for table `djangocms_picture_picture`
--
ALTER TABLE `djangocms_picture_picture`
  ADD CONSTRAINT `django_thumbnail_options_id_52a42d7a_fk_filer_thumbnailoption_id` FOREIGN KEY (`thumbnail_options_id`) REFERENCES `filer_thumbnailoption` (`id`),
  ADD CONSTRAINT `djangocms_picture_pi_cmsplugin_ptr_id_b1c2d1_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_picture_picture_id_1e9420cf_fk_filer_image_file_ptr_id` FOREIGN KEY (`picture_id`) REFERENCES `filer_image` (`file_ptr_id`),
  ADD CONSTRAINT `djangocms_picture_picture_link_page_id_39dc2dcc_fk_cms_page_id` FOREIGN KEY (`link_page_id`) REFERENCES `cms_page` (`id`);

--
-- Constraints for table `djangocms_snippet_snippetptr`
--
ALTER TABLE `djangocms_snippet_snippetptr`
  ADD CONSTRAINT `djangocms_sn_snippet_id_4f251ac3_fk_djangocms_snippet_snippet_id` FOREIGN KEY (`snippet_id`) REFERENCES `djangocms_snippet_snippet` (`id`),
  ADD CONSTRAINT `djangocms_snippet__cmsplugin_ptr_id_4317d703_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_teaser_teaser`
--
ALTER TABLE `djangocms_teaser_teaser`
  ADD CONSTRAINT `djangocms_teaser_t_cmsplugin_ptr_id_4000a68e_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_teaser_teaser_page_link_id_716a6f2b_fk_cms_page_id` FOREIGN KEY (`page_link_id`) REFERENCES `cms_page` (`id`);

--
-- Constraints for table `djangocms_text_ckeditor_text`
--
ALTER TABLE `djangocms_text_ckeditor_text`
  ADD CONSTRAINT `djangocms_text_cke_cmsplugin_ptr_id_64ac61bc_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_twitter_twitterrecententries`
--
ALTER TABLE `djangocms_twitter_twitterrecententries`
  ADD CONSTRAINT `djangocms_twitter__cmsplugin_ptr_id_734b8614_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_twitter_twittersearch`
--
ALTER TABLE `djangocms_twitter_twittersearch`
  ADD CONSTRAINT `djangocms_twitter__cmsplugin_ptr_id_5c67bf91_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`);

--
-- Constraints for table `djangocms_video_videoplayer`
--
ALTER TABLE `djangocms_video_videoplayer`
  ADD CONSTRAINT `djangocms_video_vid_cmsplugin_ptr_id_2f2288e_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_video_vid_poster_id_c0280a0_fk_filer_image_file_ptr_id` FOREIGN KEY (`poster_id`) REFERENCES `filer_image` (`file_ptr_id`);

--
-- Constraints for table `djangocms_video_videosource`
--
ALTER TABLE `djangocms_video_videosource`
  ADD CONSTRAINT `djangocms_video_vi_cmsplugin_ptr_id_4c601732_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_video_videoso_source_file_id_12bf15dc_fk_filer_file_id` FOREIGN KEY (`source_file_id`) REFERENCES `filer_file` (`id`);

--
-- Constraints for table `djangocms_video_videotrack`
--
ALTER TABLE `djangocms_video_videotrack`
  ADD CONSTRAINT `djangocms_video_vi_cmsplugin_ptr_id_2dc34265_fk_cms_cmsplugin_id` FOREIGN KEY (`cmsplugin_ptr_id`) REFERENCES `cms_cmsplugin` (`id`),
  ADD CONSTRAINT `djangocms_video_videotrack_src_id_33c3831d_fk_filer_file_id` FOREIGN KEY (`src_id`) REFERENCES `filer_file` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `easy_thumbnails_thumbnail`
--
ALTER TABLE `easy_thumbnails_thumbnail`
  ADD CONSTRAINT `easy_thumbnails__source_id_4b310d48_fk_easy_thumbnails_source_id` FOREIGN KEY (`source_id`) REFERENCES `easy_thumbnails_source` (`id`);

--
-- Constraints for table `easy_thumbnails_thumbnaildimensions`
--
ALTER TABLE `easy_thumbnails_thumbnaildimensions`
  ADD CONSTRAINT `easy_thumb_thumbnail_id_3f752e30_fk_easy_thumbnails_thumbnail_id` FOREIGN KEY (`thumbnail_id`) REFERENCES `easy_thumbnails_thumbnail` (`id`);

--
-- Constraints for table `filer_clipboard`
--
ALTER TABLE `filer_clipboard`
  ADD CONSTRAINT `filer_clipboard_user_id_573b6bc4_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `filer_clipboarditem`
--
ALTER TABLE `filer_clipboarditem`
  ADD CONSTRAINT `filer_clipboarditem_clipboard_id_3625ceaf_fk_filer_clipboard_id` FOREIGN KEY (`clipboard_id`) REFERENCES `filer_clipboard` (`id`),
  ADD CONSTRAINT `filer_clipboarditem_file_id_1cb6af85_fk_filer_file_id` FOREIGN KEY (`file_id`) REFERENCES `filer_file` (`id`);

--
-- Constraints for table `filer_file`
--
ALTER TABLE `filer_file`
  ADD CONSTRAINT `filer_fi_polymorphic_ctype_id_383eb7f4_fk_django_content_type_id` FOREIGN KEY (`polymorphic_ctype_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `filer_file_folder_id_4afc979b_fk_filer_folder_id` FOREIGN KEY (`folder_id`) REFERENCES `filer_folder` (`id`),
  ADD CONSTRAINT `filer_file_owner_id_658b37e1_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `filer_folder`
--
ALTER TABLE `filer_folder`
  ADD CONSTRAINT `filer_folder_owner_id_5d73fc39_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `filer_folder_parent_id_284648f5_fk_filer_folder_id` FOREIGN KEY (`parent_id`) REFERENCES `filer_folder` (`id`);

--
-- Constraints for table `filer_folderpermission`
--
ALTER TABLE `filer_folderpermission`
  ADD CONSTRAINT `filer_folderpermission_folder_id_26643568_fk_filer_folder_id` FOREIGN KEY (`folder_id`) REFERENCES `filer_folder` (`id`),
  ADD CONSTRAINT `filer_folderpermission_group_id_81323fc_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `filer_folderpermission_user_id_521cc5a1_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `filer_image`
--
ALTER TABLE `filer_image`
  ADD CONSTRAINT `filer_image_file_ptr_id_3e0b8ddc_fk_filer_file_id` FOREIGN KEY (`file_ptr_id`) REFERENCES `filer_file` (`id`);

--
-- Constraints for table `Price`
--
ALTER TABLE `Price`
  ADD CONSTRAINT `Price_ibfk_1` FOREIGN KEY (`id_service`) REFERENCES `Services` (`id_service`),
  ADD CONSTRAINT `Price_ibfk_2` FOREIGN KEY (`id_staff`) REFERENCES `Staff` (`id_staff`);

--
-- Constraints for table `Staff_position`
--
ALTER TABLE `Staff_position`
  ADD CONSTRAINT `Staff_position_ibfk_1` FOREIGN KEY (`id_position`) REFERENCES `Position` (`id_position`),
  ADD CONSTRAINT `Staff_position_ibfk_2` FOREIGN KEY (`id_staff`) REFERENCES `Staff` (`id_staff`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
